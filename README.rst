.. -*- restructuredtext -*-

=========
cpprisc16
=========
C++ library to easily write and test "assembly" code for RiSC-16 (Ridiculously Simple Computer).
(And an additional "compiler" from "extended" assembler to RiSC-16.)

(See `The RiSC-16 Architecture`_ by Bruce Jacob.)

.. _`The RiSC-16 Architecture`: https://www.ece.umd.edu/~blj/RiSC/

* cpprisc16_ – the **C++ library** implementing the RiSC-16 instructions set
* `xasmrisc16.py`_ – an additional "compiler" from "extended" assembler to RiSC-16

.. _cpprisc16: https://bitbucket.org/OPiMedia/cpprisc16/src/master/cpprisc16/
.. _`xasmrisc16.py`: https://bitbucket.org/OPiMedia/cpprisc16/src/master/xasmrisc16_py/

|



Author: 🌳 Olivier Pirson — OPi |OPi| 🇧🇪🇫🇷🇬🇧 🐧 👨‍💻 👨‍🔬
=================================================================
🌐 Website: http://www.opimedia.be/

💾 Bitbucket: https://bitbucket.org/OPiMedia/

* 📧 olivier.pirson.opi@gmail.com
* Mastodon: https://mamot.fr/@OPiMedia — Twitter: https://twitter.com/OPirson
* 👨‍💻 LinkedIn: https://www.linkedin.com/in/olivierpirson/ — CV: http://www.opimedia.be/CV/English.html
* other profiles: http://www.opimedia.be/about/

.. |OPi| image:: http://www.opimedia.be/_png/OPi.png

|



License: GPLv3_ |GPLv3|
=======================
Copyright (C) 2017, 2019, 2020 Olivier Pirson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.

.. _GPLv3: https://www.gnu.org/licenses/gpl-3.0.html

.. |GPLv3| image:: https://www.gnu.org/graphics/gplv3-88x31.png



|cpprisc16|

.. |cpprisc16| image:: https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2020/Jun/16/1784096162-6-cpprisc16-logo_avatar.png
