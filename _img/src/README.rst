.. -*- restructuredtext -*-

===============
Original images
===============

* Leibniz binary system 1697: https://simple.wikipedia.org/wiki/File:Leibniz_binary_system_1697.jpg
* NAND ANSI Labelled: https://commons.wikimedia.org/wiki/File:NAND_ANSI_Labelled.svg
