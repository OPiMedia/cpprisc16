/* -*- coding: latin-1 -*- */

/** \file tests/test__cppis2.hpp (March 15, 2017)
 *
 * Piece of cpprisc16.
 * https://bitbucket.org/OPiMedia/cpprisc16
 *
 * GPLv3 --- Copyright (C) 2017 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <cxxtest/TestSuite.h>  // CxxTest http://cxxtest.com/

#include "../cpprisc16/cppis2.hpp"

#include <cmath>

#include <iostream>


using namespace cpprisc16;


const bool HEAVY = false;


#define SHOW_FUNC_NAME {                                        \
    std::cout << std::endl                                      \
              << "=== " << __func__ << " ===" << std::endl;     \
    std::cout.flush();                                          \
  }


class Test__common : public CxxTest::TestSuite {
public:
  void test__is2_add() {
    SHOW_FUNC_NAME;

    p_movi(2, 0x666);
    p_movi(3, 0x42);
    TS_ASSERT_EQUALS(is2_add(1, 2, 3), false);
    TS_ASSERT_EQUALS(registers[1], 0x6A8);

    p_movi(2, 0xFFF0);
    p_movi(3, 0xF);
    TS_ASSERT_EQUALS(is2_add(1, 2, 3), false);
    TS_ASSERT_EQUALS(registers[1], 0xFFFF);

    p_movi(2, 0xF);
    p_movi(3, 0xFFF0);
    TS_ASSERT_EQUALS(is2_add(1, 2, 3), false);
    TS_ASSERT_EQUALS(registers[1], 0xFFFF);

    p_movi(2, 0x1234);
    p_movi(3, 0x1234);
    TS_ASSERT_EQUALS(is2_add(1, 2, 3), false);
    TS_ASSERT_EQUALS(registers[1], 0x2468);


    p_movi(2, 0xFFF1);
    p_movi(3, 0xF);
    TS_ASSERT_EQUALS(is2_add(1, 2, 3), true);
    TS_ASSERT_EQUALS(registers[1], 0x0);

    p_movi(2, 0xF);
    p_movi(3, 0xFFF1);
    TS_ASSERT_EQUALS(is2_add(1, 2, 3), true);
    TS_ASSERT_EQUALS(registers[1], 0x0);

    p_movi(2, 0xFFF0);
    p_movi(3, 0x42);
    TS_ASSERT_EQUALS(is2_add(1, 2, 3), true);
    TS_ASSERT_EQUALS(registers[1], 0x32);

    p_movi(2, 0xFFFF);
    p_movi(3, 0xFFFF);
    TS_ASSERT_EQUALS(is2_add(1, 2, 3), true);
    TS_ASSERT_EQUALS(registers[1], 0xFFFE);

    for (unsigned int a_value = 0; a_value <= 0xFFFF; ++a_value) {
      for (unsigned int b_value = 0; b_value <= 0xFF; ++b_value) {
        clear_registers();
        p_movi(2, a_value);
        p_movi(3, b_value);

        const word16_t result_value = registers[2] + registers[3];
        const bool overflow = (registers[2] > 0xFFFF - registers[3]);

        TS_ASSERT_EQUALS(is2_add(1, 2, 3), overflow);

        TS_ASSERT_EQUALS(registers[1], result_value);
      }
    }

    for (unsigned int result = 1; result < nb_registers; ++result) {
      for (unsigned int a_value = 0; a_value <= 0xFF; ++a_value) {
        for (unsigned int a = 0; a < nb_registers; ++a) {
          for (unsigned int b_value = 0; b_value <= 0xFF; ++b_value) {
            for (unsigned int b = 0; b < nb_registers; ++b) {
              clear_registers();
              p_movi(a, a_value);
              p_movi(b, b_value);

              const word16_t result_value = registers[a] + registers[b];
              const bool overflow = (registers[a] > 0xFFFF - registers[b]);

              TS_ASSERT_EQUALS(is2_add(result, a, b), overflow);

              TS_ASSERT_EQUALS(registers[result], result_value);
            }
          }
        }
      }
    }
  }


  void test__is2_add_bo() {
    SHOW_FUNC_NAME;

    for (unsigned int a_value = 0; a_value <= 0xFFFF; ++a_value) {
      for (unsigned int b_value = 0; b_value <= 0xFF; ++b_value) {
        clear_registers();
        p_movi(2, a_value);
        p_movi(3, b_value);

        const word16_t result_value = registers[2] + registers[3];
        const bool overflow = (registers[2] > 0xFFFF - registers[3]);

        is2_add_bo(1, 2, 3, over);
        TS_ASSERT(!overflow);

        goto not_over;

      over:
        TS_ASSERT(overflow);
      not_over:

        TS_ASSERT_EQUALS(registers[1], result_value);
      }
    }
  }


  void test__is2_bl() {
    SHOW_FUNC_NAME;

    for (unsigned int a_value = 0; a_value <= 0xFFFF; ++a_value) {
      for (unsigned int b_value = 0; b_value <= 0xFF; ++b_value) {
        clear_registers();
        p_movi(1, a_value);
        p_movi(2, b_value);

        is2_bl(1, 2, lower1);
        TS_ASSERT(registers[1] >= registers[2]);

        continue;

      lower1:
        TS_ASSERT(registers[1] < registers[2]);
      }

      for (unsigned int b_value = 0; b_value <= 0xFF; ++b_value) {
        clear_registers();
        p_movi(1, b_value);
        p_movi(2, a_value);

        is2_bl(1, 2, lower2);
        TS_ASSERT(registers[1] >= registers[2]);

        continue;

      lower2:
        TS_ASSERT(registers[1] < registers[2]);
      }
    }
  }


  void test__is2_mul() {
    SHOW_FUNC_NAME;

    p_movi(3, 0x666);
    p_movi(4, 0x42);
    is2_mul(2, 3, 4);
    TS_ASSERT_EQUALS(registers[1], 0x1);
    TS_ASSERT_EQUALS(registers[2], 0xA64C);

    p_movi(3, 0x6789);
    p_movi(4, 0xFEDC);
    is2_mul(2, 3, 4);
    TS_ASSERT_EQUALS(registers[1], 0x6712);
    TS_ASSERT_EQUALS(registers[2], 0xE7BC);

    p_movi(3, 0xFEDC);
    p_movi(4, 0x6789);
    is2_mul(2, 3, 4);
    TS_ASSERT_EQUALS(registers[1], 0x6712);
    TS_ASSERT_EQUALS(registers[2], 0xE7BC);

    p_movi(3, 0xFFFF);
    p_movi(4, 0xFFFF);
    is2_mul(2, 3, 4);
    TS_ASSERT_EQUALS(registers[1], 0xFFFE);
    TS_ASSERT_EQUALS(registers[2], 0x1);

    for (unsigned int a_value = 0; a_value <= 0xFFFF; ++a_value) {
      for (unsigned int b_value = 0; b_value <= 0xFF; ++b_value) {
        clear_registers();
        p_movi(3, a_value);
        p_movi(4, b_value);

        const word16_t least = registers[3]*registers[4];
        const std::uint32_t great
          = ((static_cast<std::uint32_t>(registers[3])
              * static_cast<std::uint32_t>(registers[4])) - least) >> 16;

        is2_mul(2, 3, 4);

        TS_ASSERT_EQUALS(registers[1], great);
        TS_ASSERT_EQUALS(registers[2], least);
      }
    }
  }


  void test__is2_nor() {
    SHOW_FUNC_NAME;

    p_movi(2, 0);
    p_movi(3, 0);
    is2_nor(1, 2, 3);
    TS_ASSERT_EQUALS(registers[1], 0xFFFF);

    p_movi(2, 0);
    p_movi(3, 0xFFFF);
    is2_nor(1, 2, 3);
    TS_ASSERT_EQUALS(registers[1], 0);

    p_movi(2, 0xBA98);
    p_movi(3, 0x1234);
    is2_nor(1, 2, 3);
    TS_ASSERT_EQUALS(registers[1], 0x4543);

    const unsigned int b_value_limit = (HEAVY
                                        ? 0xFFF
                                        : 0x7F);
      
    for (unsigned int a_value = 0; a_value <= 0xFFFF; ++a_value) {
      for (unsigned int b_value = 0; b_value < b_value_limit; ++b_value) {
        clear_registers();
        p_movi(2, a_value);
        p_movi(3, b_value);

        const word16_t result_value = registers[2] | registers[3];

        is2_nor(1, 2, 3);

        TS_ASSERT_EQUALS(static_cast<word16_t>(~registers[1]), result_value);
      }
    }

    for (unsigned int result = 1; result < nb_registers; ++result) {
      const unsigned int b_value_limit = (HEAVY
                                          ? 0xFFF
                                          : 0x7F);
      
      for (unsigned int a_value = 0; a_value <= b_value_limit; ++a_value) {
        for (unsigned int a = 0; a < nb_registers; ++a) {
          for (unsigned int b_value = 0; b_value <= 0xFF; ++b_value) {
            for (unsigned int b = 0; b < nb_registers; ++b) {
              clear_registers();
              p_movi(a, a_value);
              p_movi(b, b_value);

              const word16_t result_value = registers[a] | registers[b];

              is2_nor(result, a, b);

              TS_ASSERT_EQUALS(static_cast<word16_t>(~registers[result]),
                               result_value);
            }
          }
        }
      }
    }
  }


  void test__is2_sha() {
    p_movi(2, 0x1234);
    p_movi(3, 1);
    TS_ASSERT_EQUALS(is2_sha(1, 2, 3), false);
    TS_ASSERT_EQUALS(registers[1], 0x2468);

    p_movi(2, 0x7FFF);
    p_movi(3, 1);
    TS_ASSERT_EQUALS(is2_sha(1, 2, 3), false);
    TS_ASSERT_EQUALS(registers[1], 0xFFFE);

    p_movi(2, 0x8000);
    p_movi(3, 1);
    TS_ASSERT_EQUALS(is2_sha(1, 2, 3), true);
    TS_ASSERT_EQUALS(registers[1], 0);

    p_movi(2, 0x4321);
    p_movi(3, 14);
    TS_ASSERT_EQUALS(is2_sha(1, 2, 3), true);
    TS_ASSERT_EQUALS(registers[1], 0x4000);

    p_movi(2, 0x4321);
    p_movi(3, 15);
    TS_ASSERT_EQUALS(is2_sha(1, 2, 3), true);
    TS_ASSERT_EQUALS(registers[1], 0x8000);

    p_movi(2, 0x1234);
    p_movi(3, 16);
    TS_ASSERT_EQUALS(is2_sha(1, 2, 3), true);
    TS_ASSERT_EQUALS(registers[1], 0);

    p_movi(2, 0xFFFF);
    p_movi(3, 1);
    TS_ASSERT_EQUALS(is2_sha(1, 2, 3), true);
    TS_ASSERT_EQUALS(registers[1], 0xFFFE);


    p_movi(2, 0x1234);
    p_movi(3, -1);
    TS_ASSERT_EQUALS(is2_sha(1, 2, 3), false);
    TS_ASSERT_EQUALS(registers[1], 0x91A);

    p_movi(2, 0x4321);
    p_movi(3, -1);
    TS_ASSERT_EQUALS(is2_sha(1, 2, 3), true);
    TS_ASSERT_EQUALS(registers[1], 0x2190);

    p_movi(2, 0xFFFF);
    p_movi(3, -1);
    TS_ASSERT_EQUALS(is2_sha(1, 2, 3), true);
    TS_ASSERT_EQUALS(registers[1], 0xFFFF);


    for (unsigned int a_value = 0; a_value <= 0xFFFF; ++a_value) {
      for (unsigned int b_value = 0; b_value < 0xFF; ++b_value) {
        {
          // Left
          clear_registers();
          p_movi(2, a_value);
          p_movi(3, b_value);

          const std::uint32_t result_value
            = (registers[3] < 16
               ? static_cast<std::uint32_t>(registers[2]) << registers[3]
               : 0);

          const bool overflow = (registers[3] < 16
                                 ? result_value >> 16
                                 : registers[2]) != 0;

          TS_ASSERT_EQUALS(is2_sha(1, 2, 3), overflow);

          TS_ASSERT_EQUALS(registers[1], static_cast<word16_t>(result_value));
        }
        {
          // Right
          clear_registers();
          p_movi(2, a_value);
          p_movi(3, -b_value);

          const word16_t result_value
            = (b_value < 16
               ? static_cast<std::int16_t>(registers[2]) >> b_value
               : 0);

          const bool overflow
            = (b_value < 16
               ? static_cast<std::int16_t>(registers[2] << (16 - b_value))
               : registers[2]) != 0;

          TS_ASSERT_EQUALS(is2_sha(1, 2, 3), overflow);

          TS_ASSERT_EQUALS(registers[1], result_value);
        }
      }
    }
  }


  void test__is2_sha_bo() {
    SHOW_FUNC_NAME;

    for (unsigned int a_value = 0; a_value <= 0xFFFF; ++a_value) {
      for (unsigned int b_value = 0; b_value < 0xFF; ++b_value) {
        {
          // Left
          clear_registers();
          p_movi(2, a_value);
          p_movi(3, b_value);

          const std::uint32_t result_value
            = (registers[3] < 16
               ? static_cast<std::uint32_t>(registers[2]) << registers[3]
               : 0);

          const bool overflow = (registers[3] < 16
                                 ? result_value >> 16
                                 : registers[2]) != 0;

          is2_sha_bo(1, 2, 3, over);
          TS_ASSERT(!overflow);

          goto not_over;

        over:
          TS_ASSERT(overflow);
        not_over:

          TS_ASSERT_EQUALS(registers[1], static_cast<word16_t>(result_value));
        }
        {
          // Right
          clear_registers();
          p_movi(2, a_value);
          p_movi(3, -b_value);

          const word16_t result_value
            = (b_value < 16
               ? static_cast<std::int16_t>(registers[2]) >> b_value
               : 0);

          const bool overflow
            = (b_value < 16
               ? static_cast<std::int16_t>(registers[2] << (16 - b_value))
               : registers[2]) != 0;

          is2_sha_bo(1, 2, 3, right_over);
          TS_ASSERT(!overflow);

          goto not_right_over;

        right_over:
          TS_ASSERT(overflow);
        not_right_over:

          TS_ASSERT_EQUALS(registers[1], result_value);
        }
      }
    }
  }


  void test__is2_shl() {
    SHOW_FUNC_NAME;

    p_movi(2, 0x1234);
    p_movi(3, 1);
    TS_ASSERT_EQUALS(is2_shl(1, 2, 3), false);
    TS_ASSERT_EQUALS(registers[1], 0x2468);

    p_movi(2, 0x7FFF);
    p_movi(3, 1);
    TS_ASSERT_EQUALS(is2_shl(1, 2, 3), false);
    TS_ASSERT_EQUALS(registers[1], 0xFFFE);

    p_movi(2, 0x8000);
    p_movi(3, 1);
    TS_ASSERT_EQUALS(is2_shl(1, 2, 3), true);
    TS_ASSERT_EQUALS(registers[1], 0);

    p_movi(2, 0x4321);
    p_movi(3, 14);
    TS_ASSERT_EQUALS(is2_shl(1, 2, 3), true);
    TS_ASSERT_EQUALS(registers[1], 0x4000);

    p_movi(2, 0x4321);
    p_movi(3, 15);
    TS_ASSERT_EQUALS(is2_shl(1, 2, 3), true);
    TS_ASSERT_EQUALS(registers[1], 0x8000);

    p_movi(2, 0x1234);
    p_movi(3, 16);
    TS_ASSERT_EQUALS(is2_shl(1, 2, 3), true);
    TS_ASSERT_EQUALS(registers[1], 0);

    p_movi(2, 0xFFFF);
    p_movi(3, 1);
    TS_ASSERT_EQUALS(is2_shl(1, 2, 3), true);
    TS_ASSERT_EQUALS(registers[1], 0xFFFE);


    p_movi(2, 0x1234);
    p_movi(3, -1);
    TS_ASSERT_EQUALS(is2_shl(1, 2, 3), false);
    TS_ASSERT_EQUALS(registers[1], 0x91A);

    p_movi(2, 0x4321);
    p_movi(3, -1);
    TS_ASSERT_EQUALS(is2_shl(1, 2, 3), true);
    TS_ASSERT_EQUALS(registers[1], 0x2190);

    p_movi(2, 0xFFFF);
    p_movi(3, -1);
    TS_ASSERT_EQUALS(is2_shl(1, 2, 3), true);
    TS_ASSERT_EQUALS(registers[1], 0x7FFF);


    for (unsigned int a_value = 0; a_value <= 0xFFFF; ++a_value) {
      for (unsigned int b_value = 0; b_value < 0xFF; ++b_value) {
        {
          // Left
          clear_registers();
          p_movi(2, a_value);
          p_movi(3, b_value);

          const std::uint32_t result_value
            = (registers[3] < 16
               ? static_cast<std::uint32_t>(registers[2]) << registers[3]
               : 0);

          const bool overflow = (registers[3] < 16
                                 ? result_value >> 16
                                 : registers[2]) != 0;

          TS_ASSERT_EQUALS(is2_shl(1, 2, 3), overflow);

          TS_ASSERT_EQUALS(registers[1], static_cast<word16_t>(result_value));
        }
        {
          // Right
          clear_registers();
          p_movi(2, a_value);
          p_movi(3, -b_value);

          const word16_t result_value = (b_value < 16
                                         ? registers[2] >> b_value
                                         : 0);

          const bool overflow
            = (b_value < 16
               ? static_cast<std::int16_t>(registers[2] << (16 - b_value))
               : registers[2]) != 0;

          TS_ASSERT_EQUALS(is2_shl(1, 2, 3), overflow);

          TS_ASSERT_EQUALS(registers[1], result_value);
        }
      }
    }
  }


  void test__is2_shl_bo() {
    SHOW_FUNC_NAME;

    for (unsigned int a_value = 0; a_value <= 0xFFFF; ++a_value) {
      for (unsigned int b_value = 0; b_value < 0xFF; ++b_value) {
        {
          // Left
          clear_registers();
          p_movi(2, a_value);
          p_movi(3, b_value);

          const std::uint32_t result_value
            = (static_cast<std::uint32_t>(registers[3]) < 16
               ? registers[2] << registers[3]
               : 0);

          const bool overflow = (registers[3] < 16
                                 ? result_value >> 16
                                 : registers[2]) != 0;

          is2_shl_bo(1, 2, 3, over);
          TS_ASSERT(!overflow);

          goto not_over;

        over:
          TS_ASSERT(overflow);
        not_over:

          TS_ASSERT_EQUALS(registers[1], static_cast<word16_t>(result_value));
        }
        {
          // Right
          clear_registers();
          p_movi(2, a_value);
          p_movi(3, -b_value);

          const word16_t result_value = (b_value < 16
                                         ? registers[2] >> b_value
                                         : 0);

          const bool overflow
            = (b_value < 16
               ? static_cast<std::int16_t>(registers[2] << (16 - b_value))
               : registers[2]) != 0;

          is2_shl_bo(1, 2, 3, right_over);
          TS_ASSERT(!overflow);

          goto not_right_over;

        right_over:
          TS_ASSERT(overflow);
        not_right_over:

          TS_ASSERT_EQUALS(registers[1], result_value);
        }
      }
    }
  }


  void test__is2_shifti() {
    SHOW_FUNC_NAME;

    p_movi(2, 0x1234);
    is2_shifti(1, 2, 1);
    TS_ASSERT_EQUALS(registers[1], 0x2468);

    p_movi(2, 0x7FFF);
    is2_shifti(1, 2, 1);
    TS_ASSERT_EQUALS(registers[1], 0xFFFE);

    p_movi(2, 0x8000);
    is2_shifti(1, 2, 1);
    TS_ASSERT_EQUALS(registers[1], 0);

    p_movi(2, 0x4321);
    is2_shifti(1, 2, 14);
    TS_ASSERT_EQUALS(registers[1], 0x4000);

    p_movi(2, 0x4321);
    is2_shifti(1, 2, 15);
    TS_ASSERT_EQUALS(registers[1], 0x8000);

    p_movi(2, 0xFFFF);
    is2_shifti(1, 2, 1);
    TS_ASSERT_EQUALS(registers[1], 0xFFFE);


    // Logic >>
    p_movi(2, 0x1234);
    is2_shifti(1, 2, 0x1F);  // 0x1F == -1 on 5 bits
    TS_ASSERT_EQUALS(registers[1], 0x91A);

    p_movi(2, 0x4321);
    is2_shifti(1, 2, 0x1F);  // 0x1F == -1 on 5 bits
    TS_ASSERT_EQUALS(registers[1], 0x2190);

    p_movi(2, 0xFFFF);
    is2_shifti(1, 2, 0x1F);  // 0x1F == -1 on 5 bits
    TS_ASSERT_EQUALS(registers[1], 0x7FFF);


    // Arithmetic >>
    p_movi(2, 0x1234);
    is2_shifti(1, 2, 0x3F);  // 0x1F == -1 on 5 first bits
    TS_ASSERT_EQUALS(registers[1], 0x91A);

    p_movi(2, 0x4321);
    is2_shifti(1, 2, 0x3F);  // 0x1F == -1 on 5 first bits
    TS_ASSERT_EQUALS(registers[1], 0x2190);

    p_movi(2, 0xFFFF);
    p_movi(3, 1);
    is2_shifti(1, 2, 0x3F);  // 0x1F == -1 on 5 first bits
    TS_ASSERT_EQUALS(registers[1], 0xFFFF);


    for (unsigned int a_value = 0; a_value <= 0xFFFF; ++a_value) {
      for (unsigned int b_value = 0; b_value < 0xF; ++b_value) {
        {
          // Left
          clear_registers();
          p_movi(2, a_value);

          const word16_t result_value = registers[2] << b_value;

          // logic <<
          is2_shifti(1, 2, b_value);
          TS_ASSERT_EQUALS(registers[1], result_value);

          is2_shifti(1, 2, b_value | 0x40);
          TS_ASSERT_EQUALS(registers[1], result_value);

          // arithmetic <<
          is2_shifti(1, 2, b_value | 0x20);
          TS_ASSERT_EQUALS(registers[1], result_value);

          is2_shifti(1, 2, b_value | 0x60);
          TS_ASSERT_EQUALS(registers[1], result_value);
        }
        {
          // Right
          clear_registers();
          p_movi(2, a_value);

          { // logic >>
            const word16_t result_value = registers[2] >> b_value;

            is2_shifti(1, 2, (-b_value) & 0x1F);
            TS_ASSERT_EQUALS(registers[1], result_value);

            is2_shifti(1, 2, ((-b_value) & 0x1F) | 0x40);
            TS_ASSERT_EQUALS(registers[1], result_value);
          }

          { // arithmetic >>
            const word16_t result_value
              = static_cast<std::int16_t>(registers[2]) >> b_value;

            is2_shifti(1, 2, ((-b_value) & 0x1F) | 0x20);
            TS_ASSERT_EQUALS(registers[1], result_value);

            is2_shifti(1, 2, ((-b_value) & 0x1F) | 0x60);
            TS_ASSERT_EQUALS(registers[1], result_value);
          }
        }
      }
    }
  }


  void test__is2_sub() {
    SHOW_FUNC_NAME;

    p_movi(2, 0x666);
    p_movi(3, 0x42);
    TS_ASSERT_EQUALS(is2_sub(1, 2, 3), false);
    TS_ASSERT_EQUALS(registers[1], 0x624);

    p_movi(2, 0xF);
    p_movi(3, 0x6);
    TS_ASSERT_EQUALS(is2_sub(1, 2, 3), false);
    TS_ASSERT_EQUALS(registers[1], 0x9);

    p_movi(2, 0x6);
    p_movi(3, 0xF);
    TS_ASSERT_EQUALS(is2_sub(1, 2, 3), true);
    TS_ASSERT_EQUALS(registers[1], 0xFFF7);


    p_movi(2, 0xFFF0);
    p_movi(3, 0x42);
    TS_ASSERT_EQUALS(is2_sub(1, 2, 3), false);
    TS_ASSERT_EQUALS(registers[1], 0xFFAE);

    p_movi(2, 0x1234);
    p_movi(3, 0x1234);
    TS_ASSERT_EQUALS(is2_sub(1, 2, 3), false);
    TS_ASSERT_EQUALS(registers[1], 0);

    for (unsigned int a_value = 0; a_value <= 0xFFFF; ++a_value) {
      for (unsigned int b_value = 0; b_value <= 0xFF; ++b_value) {
        clear_registers();
        p_movi(2, a_value);
        p_movi(3, b_value);

        const word16_t result_value = registers[2] - registers[3];
        const bool overflow = (registers[2] < registers[3]);

        TS_ASSERT_EQUALS(is2_sub(1, 2, 3), overflow);

        TS_ASSERT_EQUALS(registers[1], result_value);
      }
    }

    for (unsigned int result = 1; result < nb_registers; ++result) {
      for (unsigned int a_value = 0; a_value <= 0xFF; ++a_value) {
        for (unsigned int a = 0; a < nb_registers; ++a) {
          for (unsigned int b_value = 0; b_value <= 0xFF; ++b_value) {
            for (unsigned int b = 0; b < nb_registers; ++b) {
              clear_registers();
              p_movi(a, a_value);
              p_movi(b, b_value);

              const word16_t result_value = registers[a] - registers[b];
              const bool overflow = (registers[a] < registers[b]);

              TS_ASSERT_EQUALS(is2_sub(result, a, b), overflow);

              TS_ASSERT_EQUALS(registers[result], result_value);
            }
          }
        }
      }
    }
  }


  void test__is2_sub_bo() {
    SHOW_FUNC_NAME;

    for (unsigned int a_value = 0; a_value <= 0xFFFF; ++a_value) {
      for (unsigned int b_value = 0; b_value <= 0xFF; ++b_value) {
        clear_registers();
        p_movi(2, a_value);
        p_movi(3, b_value);

        const word16_t result_value = registers[2] - registers[3];
        const bool overflow = (registers[2] < registers[3]);

        is2_sub_bo(1, 2, 3, over);
        TS_ASSERT(!overflow);

        goto not_over;

      over:
        TS_ASSERT(overflow);
      not_over:

        TS_ASSERT_EQUALS(registers[1], result_value);
      }
    }
  }


  void test__is2_xor() {
    SHOW_FUNC_NAME;

    p_movi(2, 0);
    p_movi(3, 0);
    is2_xor(1, 2, 3);
    TS_ASSERT_EQUALS(registers[1], 0);

    p_movi(2, 0);
    p_movi(3, 0xFFFF);
    is2_xor(1, 2, 3);
    TS_ASSERT_EQUALS(registers[1], 0xFFFF);

    p_movi(2, 0xFFFF);
    p_movi(3, 0xFFFF);
    is2_xor(1, 2, 3);
    TS_ASSERT_EQUALS(registers[1], 0);

    p_movi(2, 0xBA98);
    p_movi(3, 0x1234);
    is2_xor(1, 2, 3);
    TS_ASSERT_EQUALS(registers[1], 0xA8AC);

    const unsigned int b_value_limit = (HEAVY
                                        ? 0xFFF
                                        : 0x7F);
      
    for (unsigned int a_value = 0; a_value <= 0xFFFF; ++a_value) {
      for (unsigned int b_value = 0; b_value < b_value_limit; ++b_value) {
        clear_registers();
        p_movi(2, a_value);
        p_movi(3, b_value);

        const word16_t result_value = registers[2] ^ registers[3];

        is2_xor(1, 2, 3);

        TS_ASSERT_EQUALS(registers[1], result_value);
      }
    }
  }


  void test___operations() {
    SHOW_FUNC_NAME;
    
    const unsigned int nb = 100000;

    clear_registers();
    srand(666);

    for (unsigned int i = 0; i < nb; ++i) {
      const word16_t value_a = rand();

      p_movi(1, value_a);

      { // *2
        const word16_t result_value = value_a*2;
      
        p_movi(2, 666);
        is2_add(2, 1, 1);
        TS_ASSERT_EQUALS(registers[2], result_value);

        p_movi(2, 2);
        p_movi(3, 666);
        is2_mul(3, 1, 2);
        TS_ASSERT_EQUALS(registers[2], ((value_a & 0x8000) == 0
                                        ? 0
                                        : 1));
        TS_ASSERT_EQUALS(registers[3], result_value);

        p_movi(3, 1);
        is2_shl(3, 1, 3);  // logic << 1
        TS_ASSERT_EQUALS(registers[3], result_value);

        p_movi(3, 1);
        is2_sha(3, 1, 3);  // arithmetic << 1 
        TS_ASSERT_EQUALS(registers[3], result_value);

        p_movi(3, 666);
        is2_shifti(3, 1, 1);  // logic << 1
        TS_ASSERT_EQUALS(registers[3], result_value);

        p_movi(3, 666);
        is2_shifti(3, 1, 0x21);  // arithmetic << 1 
        TS_ASSERT_EQUALS(registers[3], result_value);
      }

      { // /2
        const word16_t result_value = value_a/2;
      
        p_movi(2, -1);
        is2_shl(2, 1, 2);  // logic >> 1

        TS_ASSERT_EQUALS(registers[2], result_value);
        p_movi(2, -1);
        is2_sha(2, 1, 2);  // arithmetic >> 1 
        if ((value_a & 0x8000) == 0) {
          TS_ASSERT_EQUALS(registers[2], result_value);
        }
        else {
          TS_ASSERT_EQUALS(registers[2] & 0x8000, 0x8000);
          TS_ASSERT_EQUALS(registers[2] & 0x7FFF, result_value);
        }

        p_movi(2, 666);
        is2_shifti(2, 1, 0x1F);  // logic >> 1
        TS_ASSERT_EQUALS(registers[2], result_value);

        p_movi(2, 666);
        is2_shifti(2, 1, 0x3F);  // arithmetic >> 1
        if ((value_a & 0x8000) == 0) {
          TS_ASSERT_EQUALS(registers[2], result_value);
        }
        else {
          TS_ASSERT_EQUALS(registers[2] & 0x8000, 0x8000);
          TS_ASSERT_EQUALS(registers[2] & 0x7FFF, result_value);
        }
      }
    }
  }
};
