/* -*- coding: latin-1 -*- */

/** \file tests/test__cpprisc16.hpp (March 15, 2017)
 *
 * Piece of cpprisc16.
 * https://bitbucket.org/OPiMedia/cpprisc16
 *
 * GPLv3 --- Copyright (C) 2017 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <cxxtest/TestSuite.h>  // CxxTest http://cxxtest.com/

#include "../cpprisc16/cpprisc16.hpp"

#include <iostream>


using namespace cpprisc16;


const bool HEAVY = false;


#define SHOW_FUNC_NAME {                                        \
    std::cout << std::endl                                      \
              << "=== " << __func__ << " ===" << std::endl;     \
    std::cout.flush();                                          \
  }


class Test__common : public CxxTest::TestSuite {
public:
  void test__i_add() {
    SHOW_FUNC_NAME;

    p_movi(2, 0x666);
    p_movi(3, 0x42);
    i_add(1, 2, 3);
    TS_ASSERT_EQUALS(registers[1], 0x6A8);

    p_movi(2, 0xFFF0);
    p_movi(3, 0xF);
    i_add(1, 2, 3);
    TS_ASSERT_EQUALS(registers[1], 0xFFFF);

    p_movi(2, 0xF);
    p_movi(3, 0xFFF0);
    i_add(1, 2, 3);
    TS_ASSERT_EQUALS(registers[1], 0xFFFF);

    p_movi(2, 0x1234);
    p_movi(3, 0x1234);
    i_add(1, 2, 3);
    TS_ASSERT_EQUALS(registers[1], 0x2468);


    p_movi(2, 0xFFF1);
    p_movi(3, 0xF);
    i_add(1, 2, 3);
    TS_ASSERT_EQUALS(registers[1], 0x0);

    p_movi(2, 0xF);
    p_movi(3, 0xFFF1);
    i_add(1, 2, 3);
    TS_ASSERT_EQUALS(registers[1], 0x0);

    p_movi(2, 0xFFF0);
    p_movi(3, 0x42);
    i_add(1, 2, 3);
    TS_ASSERT_EQUALS(registers[1], 0x32);

    p_movi(2, 0xFFFF);
    p_movi(3, 0xFFFF);
    i_add(1, 2, 3);
    TS_ASSERT_EQUALS(registers[1], 0xFFFE);

    for (unsigned int a_value = 0; a_value <= 0xFFFF; ++a_value) {
      for (unsigned int b_value = 0; b_value <= 0xFF; ++b_value) {
        clear_registers();
        p_movi(2, a_value);
        p_movi(3, b_value);

        const word16_t result_value = registers[2] + registers[3];

        i_add(1, 2, 3);

        TS_ASSERT_EQUALS(registers[1], result_value);
      }
    }

    for (unsigned int result = 1; result < nb_registers; ++result) {
      for (unsigned int a_value = 0; a_value <= 0xFF; ++a_value) {
        for (unsigned int a = 0; a < nb_registers; ++a) {
          for (unsigned int b_value = 0; b_value <= 0xFF; ++b_value) {
            for (unsigned int b = 0; b < nb_registers; ++b) {
              clear_registers();
              p_movi(a, a_value);
              p_movi(b, b_value);

              const word16_t result_value = registers[a] + registers[b];

              i_add(result, a, b);

              TS_ASSERT_EQUALS(registers[result], result_value);
            }
          }
        }
      }
    }

    TS_ASSERT_EQUALS(registers[0], 0);
  }


  void test__i_addi() {
    SHOW_FUNC_NAME;

    p_movi(2, 0xFFF0);
    i_addi(1, 2, 0);
    TS_ASSERT_EQUALS(registers[1], 0xFFF0);

    p_movi(2, 0xFFF0);
    i_addi(1, 2, 0xF);
    TS_ASSERT_EQUALS(registers[1], 0xFFFF);

    p_movi(2, 0xFFF1);
    i_addi(1, 2, 0xF);
    TS_ASSERT_EQUALS(registers[1], 0);


    for (unsigned int a_value = 0; a_value <= 0xFFFF; ++a_value) {
      for (unsigned int b_value = 0; b_value <= 0x3F; ++b_value) {
        clear_registers();
        p_movi(2, a_value);

        const word16_t result_value = registers[2] + b_value;

        i_addi(1, 2, b_value);

        TS_ASSERT_EQUALS(registers[1], result_value);
      }
    }

    for (unsigned int result = 1; result < nb_registers; ++result) {
      for (unsigned int a_value = 0; a_value <= 0xFF; ++a_value) {
        for (unsigned int a = 0; a < nb_registers; ++a) {
          for (unsigned int b_value = 0; b_value <= 0x3F; ++b_value) {
            clear_registers();
            p_movi(a, a_value);

            const word16_t result_value = registers[a] + b_value;

            i_addi(result, a, b_value);

            TS_ASSERT_EQUALS(registers[result], result_value);
          }
        }
      }
    }

    TS_ASSERT_EQUALS(registers[0], 0);
  }


  void test__i_beq() {
    SHOW_FUNC_NAME;

    p_movi(1, 0x666);
    p_movi(2, 0x42);
    i_beq(1, 2, equal1);
    TS_ASSERT(true);

    goto not_equal1;

  equal1:
    TS_ASSERT(true);
  not_equal1:

    p_movi(2, 0x666);
    i_beq(1, 2, equal2);
    TS_ASSERT(false);

  equal2:
    TS_ASSERT(true);
  }


  void test__i_lui() {
    SHOW_FUNC_NAME;

    i_lui(1, 0);
    TS_ASSERT_EQUALS(registers[1], 0);

    i_lui(1, 1);
    TS_ASSERT_EQUALS(registers[1], 0x40);

    i_lui(1, 0x42);
    TS_ASSERT_EQUALS(registers[1], 0x1080);


    for (unsigned int result = 1; result < nb_registers; ++result) {
      for (unsigned int a_value = 0; a_value <= 0xFF; ++a_value) {
        for (unsigned int a = 1; a < nb_registers; ++a) {
          clear_registers();

          const word16_t result_value = a_value << 6;

          i_lui(result, a_value);

          TS_ASSERT_EQUALS(registers[result], result_value);
        }
      }
    }

    TS_ASSERT_EQUALS(registers[0], 0);
  }


  void test__i_lw_and_i_sw() {
    SHOW_FUNC_NAME;

    clear_registers();
    clear_memory();


    // Check all memory
    for (unsigned int i = 0; i < size_memory; ++i) {
      TS_ASSERT_EQUALS(memory[i], 0);
    }


    // Read cleared memory
    for (unsigned int i = 0; i < size_memory; ++i) {
      TS_ASSERT_EQUALS(memory[i], 0);

      if (i <= 0x3F) {
        i_lw(1, 0, i);
        TS_ASSERT_EQUALS(registers[1], 0);
      }

      p_movi(1, i);
      i_lw(2, 1, 0);
      TS_ASSERT_EQUALS(registers[2], 0);
    }


    // Write in all memory
    for (unsigned int i = 0; i < size_memory; ++i) {
      if (i <= 0x3F) {
        p_movi(2, i);
        i_sw(2, 0, i);
      }

      p_movi(2, i);
      i_sw(2, 2, 0);
    }


    // Read all memory
    for (unsigned int i = 0; i < size_memory; ++i) {
      if (i <= 0x3F) {
        i_lw(3, 0, i);
        TS_ASSERT_EQUALS(registers[3], i);
      }

      p_movi(2, i);
      i_lw(4, 2, 0);
      TS_ASSERT_EQUALS(registers[4], i);
    }


    // Check all memory
    for (unsigned int i = 0; i < size_memory; ++i) {
      TS_ASSERT_EQUALS(memory[i], i);
    }

    TS_ASSERT_EQUALS(registers[0], 0);
  }


  void test__i_nand() {
    SHOW_FUNC_NAME;

    const unsigned int b_value_limit = (HEAVY
                                        ? 0xFFF
                                        : 0x7F);
      
    for (unsigned int a_value = 0; a_value <= 0xFFFF; ++a_value) {
      for (unsigned int b_value = 0; b_value < b_value_limit; ++b_value) {
        clear_registers();
        p_movi(2, a_value);
        p_movi(3, b_value);

        const word16_t result_value = registers[2] & registers[3];

        i_nand(1, 2, 3);

        TS_ASSERT_EQUALS(static_cast<word16_t>(~registers[1]), result_value);
      }
    }

    for (unsigned int result = 1; result < nb_registers; ++result) {
      const unsigned int b_value_limit = (HEAVY
                                          ? 0xFFF
                                          : 0x7F);
      
      for (unsigned int a_value = 0; a_value <= b_value_limit; ++a_value) {
        for (unsigned int a = 0; a < nb_registers; ++a) {
          for (unsigned int b_value = 0; b_value <= 0xFF; ++b_value) {
            for (unsigned int b = 0; b < nb_registers; ++b) {
              clear_registers();
              p_movi(a, a_value);
              p_movi(b, b_value);

              const word16_t result_value = registers[a] & registers[b];

              i_nand(result, a, b);

              TS_ASSERT_EQUALS(static_cast<word16_t>(~registers[result]),
                               result_value);
            }
          }
        }
      }
    }

    TS_ASSERT_EQUALS(registers[0], 0);
  }


  void test__p_halt() {
    SHOW_FUNC_NAME;

    // Not covered, but it's OK.
  }


  void test__p_movi() {
    SHOW_FUNC_NAME;

    for (unsigned int value = 0; value <= 0xFFFF; ++value) {
      for (unsigned int result = 1; result < nb_registers; ++result) {
        clear_registers();

        p_movi(result, value);
        TS_ASSERT_EQUALS(registers[result], value);

        for (unsigned int i = 0; i < nb_registers; ++i) {
          if (result != i) {
            TS_ASSERT_EQUALS(registers[i], 0);
          }
        }
      }
    }

    TS_ASSERT_EQUALS(registers[0], 0);
  }


  void test__p_nop() {
    SHOW_FUNC_NAME;

    // Not covered, but it's OK.
  }

};
