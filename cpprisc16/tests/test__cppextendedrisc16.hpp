/* -*- coding: latin-1 -*- */

/** \file tests/test__cppextendedrisc16.hpp (March 16, 2017)
 *
 * Piece of cpprisc16.
 * https://bitbucket.org/OPiMedia/cpprisc16
 *
 * GPLv3 --- Copyright (C) 2017 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <cxxtest/TestSuite.h>  // CxxTest http://cxxtest.com/

#include "../cpprisc16/cppextendedrisc16.hpp"

#include <iostream>


using namespace cpprisc16;


const bool HEAVY = false;


#define SHOW_FUNC_NAME {                                        \
    std::cout << std::endl                                      \
              << "=== " << __func__ << " ===" << std::endl;     \
    std::cout.flush();                                          \
  }


class Test__common : public CxxTest::TestSuite {
 public:
  void test__x_add32() {
    SHOW_FUNC_NAME;

    clear_registers();

    p_movi(1, 0);
    p_movi(2, 0);
    p_movi(3, 0);
    p_movi(4, 0);
    x_add32(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[1], 0);
    TS_ASSERT_EQUALS(registers[2], 0);

    p_movi(1, 0);
    p_movi(2, 1);
    p_movi(3, 0);
    p_movi(4, 0);
    x_add32(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[1], 0);
    TS_ASSERT_EQUALS(registers[2], 1);

    p_movi(1, 0);
    p_movi(2, 1);
    p_movi(3, 0);
    p_movi(4, 1);
    x_add32(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[1], 0);
    TS_ASSERT_EQUALS(registers[2], 2);

    p_movi(1, 0);
    p_movi(2, 0x8000);
    p_movi(3, 0);
    p_movi(4, 0x4000);
    x_add32(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[1], 0);
    TS_ASSERT_EQUALS(registers[2], 0xC000);

    p_movi(1, 0);
    p_movi(2, 0x7FFF);
    p_movi(3, 0);
    p_movi(4, 0x7FFF);
    x_add32(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[1], 0);
    TS_ASSERT_EQUALS(registers[2], 0xFFFE);

    p_movi(1, 0);
    p_movi(2, 0xFFFF);
    p_movi(3, 0);
    p_movi(4, 0xFFFF);
    x_add32(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[1], 1);
    TS_ASSERT_EQUALS(registers[2], 0xFFFE);

    p_movi(1, 0);
    p_movi(2, 0xFFFF);
    p_movi(3, 0);
    p_movi(4, 1);
    x_add32(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[1], 1);
    TS_ASSERT_EQUALS(registers[2], 0);

    p_movi(1, 0);
    p_movi(2, 0xFFFF);
    p_movi(3, 0);
    p_movi(4, 2);
    x_add32(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[1], 1);
    TS_ASSERT_EQUALS(registers[2], 1);

    p_movi(1, 0x7FFF);
    p_movi(2, 0x1000);
    p_movi(3, 0);
    p_movi(4, 0xF001);
    x_add32(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[1], 0x8000);
    TS_ASSERT_EQUALS(registers[2], 1);

    p_movi(1, 0xFFFF);
    p_movi(2, 0xFFFF);
    p_movi(3, 0);
    p_movi(4, 1);
    x_add32(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[1], 0);
    TS_ASSERT_EQUALS(registers[2], 0);

    p_movi(1, 0xFFFF);
    p_movi(2, 0xFFFE);
    p_movi(3, 0);
    p_movi(4, 1);
    x_add32(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[1], 0xFFFF);
    TS_ASSERT_EQUALS(registers[2], 0xFFFF);

    p_movi(1, 0x7FFF);
    p_movi(2, 0xFFFF);
    p_movi(3, 0x7FFF);
    p_movi(4, 0xFFFF);
    x_add32(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[1], 0xFFFF);
    TS_ASSERT_EQUALS(registers[2], 0xFFFE);

    p_movi(1, 0xFFFF);
    p_movi(2, 0xFFFF);
    p_movi(3, 0xFFFF);
    p_movi(4, 0xFFFF);
    x_add32(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[1], 0xFFFF);
    TS_ASSERT_EQUALS(registers[2], 0xFFFE);

    const unsigned int value_limit = (HEAVY
                                      ? 0xFF
                                      : 0xF);
      
    for (unsigned int a2_value = 0; a2_value <= 0xFF; ++a2_value) {
      for (unsigned int a1_value = 0; a1_value <= value_limit; ++a1_value) {
        for (unsigned int b2_value = 0; b2_value <= 0xFF; ++b2_value) {
          for (unsigned int b1_value = 0; b1_value <= value_limit; ++b1_value) { 
            clear_registers();
            p_movi(1, a2_value);
            p_movi(2, a1_value);
            p_movi(3, b2_value);
            p_movi(4, b1_value);

            const uint32_t result_value32
              = (static_cast<uint32_t>(registers[1]) << 16) + registers[2]
              + (static_cast<uint32_t>(registers[3]) << 16) + registers[4];

            x_add32(1, 2, 3, 4, 5, 6, 7);

            TS_ASSERT_EQUALS(registers[1],
                             static_cast<word16_t>(result_value32 >> 16));
            TS_ASSERT_EQUALS(registers[2],
                             static_cast<word16_t>(result_value32));
          }
        }
      }
    }
  }


  void test__x_addc() {
    SHOW_FUNC_NAME;

    clear_registers();

    p_movi(1, 0);
    p_movi(2, 0);
    x_addc(1, 2, 3, 4, 5);
    TS_ASSERT_EQUALS(registers[2], 0);
    TS_ASSERT_EQUALS(registers[1], 0);

    p_movi(1, 1);
    p_movi(2, 0);
    x_addc(1, 2, 3, 4, 5);
    TS_ASSERT_EQUALS(registers[2], 0);
    TS_ASSERT_EQUALS(registers[1], 1);

    p_movi(1, 1);
    p_movi(2, 1);
    x_addc(1, 2, 3, 4, 5);
    TS_ASSERT_EQUALS(registers[2], 0);
    TS_ASSERT_EQUALS(registers[1], 2);

    p_movi(1, 0x8000);
    p_movi(2, 0x4000);
    x_addc(1, 2, 3, 4, 5);
    TS_ASSERT_EQUALS(registers[2], 0);
    TS_ASSERT_EQUALS(registers[1], 0xC000);

    p_movi(1, 0x7FFF);
    p_movi(2, 0x7FFF);
    x_addc(1, 2, 3, 4, 5);
    TS_ASSERT_EQUALS(registers[2], 0);
    TS_ASSERT_EQUALS(registers[1], 0xFFFE);

    p_movi(1, 0xFFFF);
    p_movi(2, 0xFFFF);
    x_addc(1, 2, 3, 4, 5);
    TS_ASSERT_EQUALS(registers[2], 1);
    TS_ASSERT_EQUALS(registers[1], 0xFFFE);

    p_movi(1, 0xFFFF);
    p_movi(2, 1);
    x_addc(1, 2, 3, 4, 5);
    TS_ASSERT_EQUALS(registers[2], 1);
    TS_ASSERT_EQUALS(registers[1], 0);

    p_movi(1, 0xFFFF);
    p_movi(2, 2);
    x_addc(1, 2, 3, 4, 5);
    TS_ASSERT_EQUALS(registers[2], 1);
    TS_ASSERT_EQUALS(registers[1], 1);

    p_movi(1, 0xF000);
    p_movi(2, 0);
    x_addc(1, 2, 3, 4, 5);
    TS_ASSERT_EQUALS(registers[2], 0);
    TS_ASSERT_EQUALS(registers[1], 0xF000);

    p_movi(1, 0xF000);
    p_movi(2, 0xF000);
    x_addc(1, 2, 3, 4, 5);
    TS_ASSERT_EQUALS(registers[2], 1);
    TS_ASSERT_EQUALS(registers[1], 0xE000);

    p_movi(1, 0xFFFF);
    p_movi(2, 0xFFFF);
    x_addc(1, 2, 3, 4, 5);
    TS_ASSERT_EQUALS(registers[2], 1);
    TS_ASSERT_EQUALS(registers[1], 0xFFFE);

    const unsigned int b_value_limit = (HEAVY
                                        ? 0xFFF
                                        : 0x3F);
      
    for (unsigned int a_value = 0; a_value <= 0xFFFF; ++a_value) {
      for (unsigned int b_value = 0; b_value <= b_value_limit; ++b_value) { 
        clear_registers();
        p_movi(1, a_value);
        p_movi(2, b_value);

        const uint32_t result_value32
          = static_cast<uint32_t>(registers[1]) + registers[2];

        x_addc(1, 2, 3, 4, 5);

        TS_ASSERT_EQUALS(registers[2],
                         static_cast<word16_t>(result_value32 >> 16));
        TS_ASSERT_EQUALS(registers[1],
                         static_cast<word16_t>(result_value32));
      }
    }
  }


  void test__x_and_to() {
    SHOW_FUNC_NAME;

    const unsigned int b_value_limit = (HEAVY
                                        ? 0xFFF
                                        : 0x3F);
      
    for (unsigned int a_value = 0; a_value <= 0xFFFF; ++a_value) {
      for (unsigned int b_value = 0; b_value <= b_value_limit; ++b_value) { 
        clear_registers();
        p_movi(2, a_value);
        p_movi(3, b_value);

        const word16_t result_value = registers[2] & registers[3];

        x_and_to(1, 2, 3);

        TS_ASSERT_EQUALS(registers[1], result_value);
      }
    }

    for (unsigned int result = 1; result < nb_registers; ++result) {
      for (unsigned int a_value = 0; a_value <= 0xFF; ++a_value) {
        for (unsigned int a = 1; a < nb_registers; ++a) {
          for (unsigned int b_value = 0; b_value <= b_value_limit; ++b_value) {
            for (unsigned int b = 1; b < nb_registers; ++b) {
              clear_registers();
              p_movi(a, a_value);
              p_movi(b, b_value);

              const word16_t result_value = registers[a] & registers[b];

              x_and_to(result, a, b);

              TS_ASSERT_EQUALS(registers[result], result_value);
            }
          }
        }
      }
    }
  }


  void test__x_inc32() {
    SHOW_FUNC_NAME;

    clear_registers();

    for (unsigned int a2_value = 0; a2_value <= 0xFF; ++a2_value) {
      for (unsigned int a1_value = 0; a1_value <= 0xFFFF; ++a1_value) {
        clear_registers();
        p_movi(2, a2_value);
        p_movi(1, a1_value);

        const uint32_t result_value32
          = ((static_cast<uint32_t>(registers[2]) << 16) | registers[1]) + 1;

        x_inc32(2, 1);

        TS_ASSERT_EQUALS(registers[2], static_cast<word16_t>(result_value32 >> 16));
        TS_ASSERT_EQUALS(registers[1], static_cast<word16_t>(result_value32));
      }
    }
  }


  void test__x_is_lower_to() {
    SHOW_FUNC_NAME;

    const unsigned int a_value_step = (HEAVY
                                       ? 0xFFFF
                                       : 0x4321);

    for (unsigned int a_value = 0; a_value <= a_value_step; ++a_value) {
      for (unsigned int b_value = 0xFF; b_value <= 0x1FF; ++b_value) { 
        clear_registers();
        p_movi(2, a_value);
        p_movi(3, b_value);

        x_is_lower_to(1, 2, 3, 4);

        if (a_value < b_value) {
          TS_ASSERT_DIFFERS(registers[1], 0);
        }
        else {
          TS_ASSERT_EQUALS(registers[1], 0);
        }

        p_movi(2, a_value);
        p_movi(3, b_value);

        x_is_lower_to(1, 3, 2, 4);

        if (a_value > b_value) {
          TS_ASSERT_DIFFERS(registers[1], 0);
        }
        else {
          TS_ASSERT_EQUALS(registers[1], 0);
        }
      }
    }
  }


  void test__x_lshift() {
    SHOW_FUNC_NAME;

    for (unsigned int a_value = 0; a_value <= 0xFFFF; ++a_value) {
      clear_registers();
      p_movi(1, a_value);

      const word16_t result_value = (registers[1] << 1);

      x_lshift(1);

      TS_ASSERT_EQUALS(registers[1], result_value);
    }

    for (unsigned int result = 1; result < nb_registers; ++result) {
      for (unsigned int a_value = 0; a_value <= 0xFFFF; ++a_value) {
        for (unsigned int a = 1; a < nb_registers; ++a) {
          clear_registers();
          p_movi(a, a_value);

          const word16_t result_value = (registers[a] << 1);

          x_lshift(a);

          TS_ASSERT_EQUALS(registers[a], result_value);
        }
      }
    }
  }


  void test__x_lshift_to() {
    SHOW_FUNC_NAME;

    for (unsigned int a_value = 0; a_value <= 0xFFFF; ++a_value) {
      clear_registers();
      p_movi(2, a_value);

      const word16_t result_value = (registers[2] << 1);

      x_lshift_to(1, 2);

      TS_ASSERT_EQUALS(registers[1], result_value);
    }

    for (unsigned int result = 1; result < nb_registers; ++result) {
      for (unsigned int a_value = 0; a_value <= 0xFFFF; ++a_value) {
        for (unsigned int a = 1; a < nb_registers; ++a) {
          clear_registers();
          p_movi(a, a_value);

          const word16_t result_value = (registers[a] << 1);

          x_lshift_to(result, a);

          TS_ASSERT_EQUALS(registers[result], result_value);
        }
      }
    }
  }


  void test__x_lshift32() {
    SHOW_FUNC_NAME;

    clear_registers();

    p_movi(1, 0);
    p_movi(2, 0);
    x_lshift32(1, 2, 3);
    TS_ASSERT_EQUALS(registers[1], 0);
    TS_ASSERT_EQUALS(registers[2], 0);

    p_movi(1, 0);
    p_movi(2, 1);
    x_lshift32(1, 2, 3);
    TS_ASSERT_EQUALS(registers[1], 0);
    TS_ASSERT_EQUALS(registers[2], 2);

    p_movi(1, 0);
    p_movi(2, 0x8FFF);
    x_lshift32(1, 2, 3);
    TS_ASSERT_EQUALS(registers[1], 1);
    TS_ASSERT_EQUALS(registers[2], 0x1FFE);

    p_movi(1, 0);
    p_movi(2, 0x8000);
    x_lshift32(1, 2, 3);
    TS_ASSERT_EQUALS(registers[1], 1);
    TS_ASSERT_EQUALS(registers[2], 0);

    p_movi(1, 0x8000);
    p_movi(2, 0);
    x_lshift32(1, 2, 3);
    TS_ASSERT_EQUALS(registers[1], 0);
    TS_ASSERT_EQUALS(registers[2], 0);

    p_movi(1, 0x8000);
    p_movi(2, 1);
    x_lshift32(1, 2, 3);
    TS_ASSERT_EQUALS(registers[1], 0);
    TS_ASSERT_EQUALS(registers[2], 2);

    p_movi(1, 1);
    p_movi(2, 0xFFFF);
    x_lshift32(1, 2, 3);
    TS_ASSERT_EQUALS(registers[1], 3);
    TS_ASSERT_EQUALS(registers[2], 0xFFFE);

    p_movi(1, 4);
    p_movi(2, 0x2000);
    x_lshift32(1, 2, 3);
    TS_ASSERT_EQUALS(registers[1], 8);
    TS_ASSERT_EQUALS(registers[2], 0x4000);

    p_movi(1, 0xFFFF);
    p_movi(2, 0xFFFF);
    x_lshift32(1, 2, 3);
    TS_ASSERT_EQUALS(registers[1], 0xFFFF);
    TS_ASSERT_EQUALS(registers[2], 0xFFFE);

    for (uint32_t a_value = 0; a_value <= 0xFFFF; ++a_value) {
      clear_registers();
      p_movi(1, a_value >> 16);
      p_movi(2, a_value & 0xFFFF);

      const uint32_t result_value = (a_value << 1);

      x_lshift32(1, 2, 3);

      TS_ASSERT_EQUALS(registers[1],
                       static_cast<word16_t>(result_value >> 16));
      TS_ASSERT_EQUALS(registers[2],
                       static_cast<word16_t>(result_value & 0xFFFF));
    }

    for (uint32_t a_value = 0xFFFF; a_value >= 0xFFFF; a_value *= 12345) {
      clear_registers();
      p_movi(1, a_value >> 16);
      p_movi(2, a_value & 0xFFFF);

      const uint32_t result_value = (a_value << 1);

      x_lshift32(1, 2, 3);

      TS_ASSERT_EQUALS(registers[1],
                       static_cast<word16_t>(result_value >> 16));
      TS_ASSERT_EQUALS(registers[2],
                       static_cast<word16_t>(result_value & 0xFFFF));
    }
  }


  void test__x_lshift_8() {
    SHOW_FUNC_NAME;

    for (unsigned int a_value = 0; a_value <= 0xFFFF; ++a_value) {
      clear_registers();
      p_movi(2, a_value);

      const word16_t result_value = (registers[1] << 8);

      x_lshift_8(1);

      TS_ASSERT_EQUALS(registers[1], result_value);
    }
  }


  void test__x_lshift_8_to() {
    SHOW_FUNC_NAME;

    for (unsigned int a_value = 0; a_value <= 0xFFFF; ++a_value) {
      clear_registers();
      p_movi(2, a_value);

      const word16_t result_value = (registers[2] << 8);

      x_lshift_8_to(1, 2);

      TS_ASSERT_EQUALS(registers[1], result_value);
    }
  }


  void test__x_lshift32_8() {
    SHOW_FUNC_NAME;

    clear_registers();

    p_movi(1, 0);
    p_movi(2, 0);
    x_lshift32_8(1, 2, 3, 4, 5);
    TS_ASSERT_EQUALS(registers[1], 0);
    TS_ASSERT_EQUALS(registers[2], 0);

    p_movi(1, 0);
    p_movi(2, 1);
    x_lshift32_8(1, 2, 3, 4, 5);
    TS_ASSERT_EQUALS(registers[1], 0);
    TS_ASSERT_EQUALS(registers[2], 0x100);

    p_movi(1, 0);
    p_movi(2, 0x8FFF);
    x_lshift32_8(1, 2, 3, 4, 5);
    TS_ASSERT_EQUALS(registers[1], 0x8F);
    TS_ASSERT_EQUALS(registers[2], 0xFF00);

    p_movi(1, 0);
    p_movi(2, 0x8000);
    x_lshift32_8(1, 2, 3, 4, 5);
    TS_ASSERT_EQUALS(registers[1], 0x80);
    TS_ASSERT_EQUALS(registers[2], 0);

    p_movi(1, 0x8000);
    p_movi(2, 0);
    x_lshift32_8(1, 2, 3, 4, 5);
    TS_ASSERT_EQUALS(registers[1], 0);
    TS_ASSERT_EQUALS(registers[2], 0);

    p_movi(1, 0x8000);
    p_movi(2, 1);
    x_lshift32_8(1, 2, 3, 4, 5);
    TS_ASSERT_EQUALS(registers[1], 0);
    TS_ASSERT_EQUALS(registers[2], 0x100);

    p_movi(1, 1);
    p_movi(2, 0xFFFF);
    x_lshift32_8(1, 2, 3, 4, 5);
    TS_ASSERT_EQUALS(registers[1], 0x1FF);
    TS_ASSERT_EQUALS(registers[2], 0xFF00);

    p_movi(1, 4);
    p_movi(2, 0x2000);
    x_lshift32_8(1, 2, 3, 4, 5);
    TS_ASSERT_EQUALS(registers[1], 0x420);
    TS_ASSERT_EQUALS(registers[2], 0);

    p_movi(1, 0xFFFF);
    p_movi(2, 0xFFFF);
    x_lshift32_8(1, 2, 3, 4, 5);
    TS_ASSERT_EQUALS(registers[1], 0xFFFF);
    TS_ASSERT_EQUALS(registers[2], 0xFF00);

    for (uint32_t a_value = 0; a_value <= 0xFFFF; ++a_value) {
      clear_registers();
      p_movi(1, a_value >> 16);
      p_movi(2, a_value & 0xFFFF);

      const uint32_t result_value = (a_value << 8);

      x_lshift32_8(1, 2, 3, 4, 5);

      TS_ASSERT_EQUALS(registers[1],
                       static_cast<word16_t>(result_value >> 16));
      TS_ASSERT_EQUALS(registers[2],
                       static_cast<word16_t>(result_value & 0xFFFF));
    }

    for (uint32_t a_value = 0xFFFF; a_value >= 0xFFFF; a_value *= 12345) {
      clear_registers();
      p_movi(1, a_value >> 16);
      p_movi(2, a_value & 0xFFFF);

      const uint32_t result_value = (a_value << 8);

      x_lshift32_8(1, 2, 3, 4, 5);

      TS_ASSERT_EQUALS(registers[1],
                       static_cast<word16_t>(result_value >> 16));
      TS_ASSERT_EQUALS(registers[2],
                       static_cast<word16_t>(result_value & 0xFFFF));
    }
  }


  void test__x_mask0x8000() {
    SHOW_FUNC_NAME;

    clear_registers();

    p_movi(1, 0);
    x_mask0x8000(1, 2);
    TS_ASSERT_EQUALS(registers[1], 0);

    p_movi(1, 0xFFF);
    x_mask0x8000(1, 2);
    TS_ASSERT_EQUALS(registers[1], 0);

    p_movi(1, 0x8000);
    x_mask0x8000(1, 2);
    TS_ASSERT_EQUALS(registers[1], 0x8000);

    p_movi(1, 0xFFFF);
    x_mask0x8000(1, 2);
    TS_ASSERT_EQUALS(registers[1], 0x8000);

    for (unsigned int a_value = 0xFFF0; a_value <= 0xFFFF; ++a_value) {
      clear_registers();
      p_movi(1, a_value);

      const word16_t result_value = registers[1] & 0x8000;

      x_mask0x8000(1, 2);

      TS_ASSERT_EQUALS(registers[1], result_value);
    }

    for (unsigned int a_value = 0; a_value <= 0xFFFF; ++a_value) {
      for (unsigned int a = 1; a < nb_registers; ++a) {
        for (unsigned int tmp = 1; tmp < nb_registers; ++tmp) {
          if (a != tmp) {
            clear_registers();
            p_movi(a, a_value);

            const word16_t result_value = registers[a] & 0x8000;

            x_mask0x8000(a, tmp);

            TS_ASSERT_EQUALS(registers[a], result_value);
          }
        }
      }
    }
  }


  void test__x_mask0x8000_to() {
    SHOW_FUNC_NAME;

    clear_registers();

    p_movi(2, 0);
    x_mask0x8000_to(1, 2);
    TS_ASSERT_EQUALS(registers[1], 0);

    p_movi(2, 0xFFF);
    x_mask0x8000_to(1, 2);
    TS_ASSERT_EQUALS(registers[1], 0);

    p_movi(2, 0x8000);
    x_mask0x8000_to(1, 2);
    TS_ASSERT_EQUALS(registers[1], 0x8000);

    p_movi(2, 0xFFFF);
    x_mask0x8000_to(1, 2);
    TS_ASSERT_EQUALS(registers[1], 0x8000);

    for (unsigned int a_value = 0xFFF0; a_value <= 0xFFFF; ++a_value) {
      clear_registers();
      p_movi(2, a_value);

      const word16_t result_value = registers[2] & 0x8000;

      x_mask0x8000_to(1, 2);

      TS_ASSERT_EQUALS(registers[1], result_value);
    }

    for (unsigned int result = 1; result < nb_registers; ++result) {
      for (unsigned int a_value = 0; a_value <= 0xFFFF; ++a_value) {
        for (unsigned int a = 1; a < nb_registers; ++a) {
          if (a == result) {
            continue;
          }

          for (unsigned int tmp = 1; tmp < nb_registers; ++tmp) {
            if ((tmp != result) &&  (tmp != a)) {
              clear_registers();
              p_movi(a, a_value);

              const word16_t result_value = registers[a] & 0x8000;

              x_mask0x8000_to(result, a);

              TS_ASSERT_EQUALS(registers[result], result_value);
            }
          }
        }
      }
    }
  }


  void test__x_mov() {
    SHOW_FUNC_NAME;

    for (unsigned int result = 1; result < nb_registers; ++result) {
      for (unsigned int a = 1; a < nb_registers; ++a) {
        clear_registers();
        p_movi(a, 666);
        x_mov(result, a);

        TS_ASSERT_EQUALS(registers[result], 666);
        TS_ASSERT_EQUALS(registers[a], 666);
      }
    }
  }


  void test__x_mul() {
    SHOW_FUNC_NAME;

    clear_registers();
    clear_memory();

    p_movi(1, 0);
    p_movi(2, 0);
    p_movi(3, 666);
    p_movi(4, 42);
    x_mul(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0);
    TS_ASSERT_EQUALS(registers[1], 0);

    p_movi(1, 0);
    p_movi(2, 0xFF);
    p_movi(3, 666);
    p_movi(4, 42);
    x_mul(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0);
    TS_ASSERT_EQUALS(registers[1], 0);

    p_movi(1, 0xFF);
    p_movi(2, 0);
    p_movi(3, 666);
    p_movi(4, 42);
    x_mul(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0);
    TS_ASSERT_EQUALS(registers[1], 0);

    p_movi(1, 0x7FFF);
    p_movi(2, 0x7);
    x_mul(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0x3);
    TS_ASSERT_EQUALS(registers[1], 0x7ff9);

    p_movi(1, 0xFFFF);
    p_movi(2, 0x7);
    x_mul(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0x6);
    TS_ASSERT_EQUALS(registers[1], 0xFFF9);

    p_movi(1, 0xA060);
    p_movi(2, 0x88DC);
    x_mul(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0x55BC);
    TS_ASSERT_EQUALS(registers[1], 0xD280);

    p_movi(1, 1);
    p_movi(2, 1);
    x_mul(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0);
    TS_ASSERT_EQUALS(registers[1], 1);

    p_movi(1, 3);
    p_movi(2, 3);
    x_mul(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0);
    TS_ASSERT_EQUALS(registers[1], 9);

    p_movi(1, 7);
    p_movi(2, 7);
    x_mul(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0);
    TS_ASSERT_EQUALS(registers[1], 0x31);

    p_movi(1, 0xF);
    p_movi(2, 0xF);
    x_mul(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0);
    TS_ASSERT_EQUALS(registers[1], 0xE1);

    p_movi(1, 0x1F);
    p_movi(2, 0x1F);
    x_mul(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0);
    TS_ASSERT_EQUALS(registers[1], 0x3C1);

    p_movi(1, 0x3F);
    p_movi(2, 0x3F);
    x_mul(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0);
    TS_ASSERT_EQUALS(registers[1], 0xF81);

    p_movi(1, 0x7F);
    p_movi(2, 0x7F);
    x_mul(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0);
    TS_ASSERT_EQUALS(registers[1], 0x3F01);

    p_movi(1, 0xFF);
    p_movi(2, 0xFF);
    x_mul(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0);
    TS_ASSERT_EQUALS(registers[1], 0xFE01);

    p_movi(1, 0x1FF);
    p_movi(2, 0x1FF);
    x_mul(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 3);
    TS_ASSERT_EQUALS(registers[1], 0xFC01);

    p_movi(1, 0x3FF);
    p_movi(2, 0x3FF);
    x_mul(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0xF);
    TS_ASSERT_EQUALS(registers[1], 0xF801);

    p_movi(1, 0x7FF);
    p_movi(2, 0x7FF);
    x_mul(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0x3F);
    TS_ASSERT_EQUALS(registers[1], 0xF001);

    p_movi(1, 0xFFF);
    p_movi(2, 0xFFF);
    x_mul(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0xFF);
    TS_ASSERT_EQUALS(registers[1], 0xE001);

    p_movi(1, 0x1FFF);
    p_movi(2, 0x1FFF);
    x_mul(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0x3FF);
    TS_ASSERT_EQUALS(registers[1], 0xC001);

    p_movi(1, 0x3FFF);
    p_movi(2, 0x3FFF);
    x_mul(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0xFFF);
    TS_ASSERT_EQUALS(registers[1], 0x8001);

    p_movi(1, 0x7FFF);
    p_movi(2, 0x7FFF);
    x_mul(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0x3FFF);
    TS_ASSERT_EQUALS(registers[1], 1);

    p_movi(1, 0xFFFF);
    p_movi(2, 0xFFFF);
    x_mul(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0xFFFE);
    TS_ASSERT_EQUALS(registers[1], 1);

    const unsigned int b_value_step = (HEAVY
                                       ? 0x321
                                       : 0x4321);

    for (uint32_t a_value = 0; a_value <= 0xFFFF; ++a_value) {
      for (uint32_t b_value = 1; b_value <= 0xFFFF; b_value += b_value_step) {
        clear_registers();
        p_movi(1, a_value);
        p_movi(2, b_value);

        const uint32_t result_value = a_value*b_value;

        x_mul(1, 2, 3, 4, 5, 6, 7);

        TS_ASSERT_EQUALS(registers[2], result_value >> 16);
        TS_ASSERT_EQUALS(registers[1], result_value & 0xFFFF);
      }
    }
  }


  void test__x_mul8_to() {
    SHOW_FUNC_NAME;

    for (unsigned int a_value = 0; a_value <= 0xFF; ++a_value) {
      for (unsigned int b_value = 0; b_value <= 0xFF; ++b_value) {
        clear_registers();
        p_movi(2, a_value);
        p_movi(3, b_value);

        const word16_t result_value = a_value*b_value;

        x_mul8_to(1, 2, 3, 4, 5);

        TS_ASSERT_EQUALS(registers[1], result_value);
      }
    }
  }


  void test__x_mul_karatsuba() {
    SHOW_FUNC_NAME;

    clear_registers();
    clear_memory();

    p_movi(1, 0);
    p_movi(2, 0);
    x_mul_karatsuba(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0);
    TS_ASSERT_EQUALS(registers[1], 0);

    p_movi(1, 0);
    p_movi(2, 0xFF);
    x_mul_karatsuba(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0);
    TS_ASSERT_EQUALS(registers[1], 0);

    p_movi(1, 0x7FFF);
    p_movi(2, 0x7);
    x_mul_karatsuba(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0x3);
    TS_ASSERT_EQUALS(registers[1], 0x7ff9);

    p_movi(1, 0xFFFF);
    p_movi(2, 0x7);
    x_mul_karatsuba(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0x6);
    TS_ASSERT_EQUALS(registers[1], 0xFFF9);

    p_movi(1, 0xA060);
    p_movi(2, 0x88DC);
    x_mul_karatsuba(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0x55BC);
    TS_ASSERT_EQUALS(registers[1], 0xD280);

    p_movi(1, 1);
    p_movi(2, 1);
    x_mul_karatsuba(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0);
    TS_ASSERT_EQUALS(registers[1], 1);

    p_movi(1, 3);
    p_movi(2, 3);
    x_mul_karatsuba(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0);
    TS_ASSERT_EQUALS(registers[1], 9);

    p_movi(1, 7);
    p_movi(2, 7);
    x_mul_karatsuba(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0);
    TS_ASSERT_EQUALS(registers[1], 0x31);

    p_movi(1, 0xF);
    p_movi(2, 0xF);
    x_mul_karatsuba(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0);
    TS_ASSERT_EQUALS(registers[1], 0xE1);

    p_movi(1, 0x1F);
    p_movi(2, 0x1F);
    x_mul_karatsuba(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0);
    TS_ASSERT_EQUALS(registers[1], 0x3C1);

    p_movi(1, 0x3F);
    p_movi(2, 0x3F);
    x_mul_karatsuba(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0);
    TS_ASSERT_EQUALS(registers[1], 0xF81);

    p_movi(1, 0x7F);
    p_movi(2, 0x7F);
    x_mul_karatsuba(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0);
    TS_ASSERT_EQUALS(registers[1], 0x3F01);

    p_movi(1, 0xFF);
    p_movi(2, 0xFF);
    x_mul_karatsuba(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0);
    TS_ASSERT_EQUALS(registers[1], 0xFE01);

    p_movi(1, 0x1FF);
    p_movi(2, 0x1FF);
    x_mul_karatsuba(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 3);
    TS_ASSERT_EQUALS(registers[1], 0xFC01);

    p_movi(1, 0x3FF);
    p_movi(2, 0x3FF);
    x_mul_karatsuba(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0xF);
    TS_ASSERT_EQUALS(registers[1], 0xF801);

    p_movi(1, 0x7FF);
    p_movi(2, 0x7FF);
    x_mul_karatsuba(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0x3F);
    TS_ASSERT_EQUALS(registers[1], 0xF001);

    p_movi(1, 0xFFF);
    p_movi(2, 0xFFF);
    x_mul_karatsuba(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0xFF);
    TS_ASSERT_EQUALS(registers[1], 0xE001);

    p_movi(1, 0x1FFF);
    p_movi(2, 0x1FFF);
    x_mul_karatsuba(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0x3FF);
    TS_ASSERT_EQUALS(registers[1], 0xC001);

    p_movi(1, 0x3FFF);
    p_movi(2, 0x3FFF);
    x_mul_karatsuba(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0xFFF);
    TS_ASSERT_EQUALS(registers[1], 0x8001);

    p_movi(1, 0x7FFF);
    p_movi(2, 0x7FFF);
    x_mul_karatsuba(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0x3FFF);
    TS_ASSERT_EQUALS(registers[1], 1);

    p_movi(1, 0xFFFF);
    p_movi(2, 0xFFFF);
    x_mul_karatsuba(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0xFFFE);
    TS_ASSERT_EQUALS(registers[1], 1);

    const unsigned int b_value_step = (HEAVY
                                       ? 0x321
                                       : 0x4321);

    for (uint32_t a_value = 0; a_value <= 0xFFFF; ++a_value) {
      for (uint32_t b_value = 1; b_value <= 0xFFFF; b_value += b_value_step) {
        clear_registers();
        p_movi(1, a_value);
        p_movi(2, b_value);

        const uint32_t result_value = a_value*b_value;

        x_mul_karatsuba(1, 2, 3, 4, 5, 6, 7);

        TS_ASSERT_EQUALS(registers[2], result_value >> 16);
        TS_ASSERT_EQUALS(registers[1], result_value & 0xFFFF);
      }
    }
  }


  void test__x_neg() {
    SHOW_FUNC_NAME;

    for (unsigned int a_value = 0; a_value <= 0xFFFF; ++a_value) {
      clear_registers();
      p_movi(1, a_value);

      const word16_t result_value = -registers[1];

      x_neg(1);

      TS_ASSERT_EQUALS(registers[1], result_value);
    }

    for (unsigned int a_value = 0; a_value <= 0xFFFF; ++a_value) {
      for (unsigned int a = 1; a < nb_registers; ++a) {
        clear_registers();
        p_movi(a, a_value);

        const word16_t result_value = -registers[a];

        x_neg(a);

        TS_ASSERT_EQUALS(registers[a], result_value);
      }
    }
  }


  void test__x_not() {
    SHOW_FUNC_NAME;

    for (unsigned int a_value = 0; a_value <= 0xFFFF; ++a_value) {
      clear_registers();
      p_movi(1, a_value);

      const word16_t result_value = ~registers[1];

      x_not(1);

      TS_ASSERT_EQUALS(registers[1], result_value);
    }

    for (unsigned int a_value = 0; a_value <= 0xFFFF; ++a_value) {
      for (unsigned int a = 1; a < nb_registers; ++a) {
        clear_registers();
        p_movi(a, a_value);

        const word16_t result_value = ~registers[a];

        x_not(a);

        TS_ASSERT_EQUALS(registers[a], result_value);
      }
    }
  }


  void test__x_not_to() {
    SHOW_FUNC_NAME;

    for (unsigned int a_value = 0; a_value <= 0xFFFF; ++a_value) {
      clear_registers();
      p_movi(2, a_value);

      const word16_t result_value = ~registers[2];

      x_not_to(1, 2);

      TS_ASSERT_EQUALS(registers[1], result_value);
    }

    for (unsigned int result = 1; result < nb_registers; ++result) {
      for (unsigned int a_value = 0; a_value <= 0xFFFF; ++a_value) {
        for (unsigned int a = 1; a < nb_registers; ++a) {
          clear_registers();
          p_movi(a, a_value);

          const word16_t result_value = ~registers[a];

          x_not_to(result, a);

          TS_ASSERT_EQUALS(registers[result], result_value);
        }
      }
    }
  }


  void test__x_or_to() {
    SHOW_FUNC_NAME;

    const unsigned int b_value_limit = (HEAVY
                                        ? 0xFFF
                                        : 0x7F);
      
    for (unsigned int a_value = 0; a_value <= 0xFFFF; ++a_value) {
      for (unsigned int b_value = 0; b_value < b_value_limit; ++b_value) {
        clear_registers();
        p_movi(2, a_value);
        p_movi(3, b_value);

        const word16_t result_value = registers[2] | registers[3];

        x_or_to(1, 2, 3);

        TS_ASSERT_EQUALS(registers[1], result_value);
      }
    }

    for (unsigned int result = 1; result < nb_registers; ++result) {
      for (unsigned int a_value = 0; a_value <= 0xFF; ++a_value) {
        for (unsigned int a = 1; a < nb_registers; ++a) {
          for (unsigned int b_value = 0; b_value <= b_value_limit; ++b_value) {
            for (unsigned int b = 1; b < nb_registers; ++b) {
              if (a != b) {
                if (a == result || b == result) continue;
                clear_registers();
                p_movi(a, a_value);
                p_movi(b, b_value);

                const word16_t result_value = registers[a] | registers[b];

                x_or_to(result, a, b);

                TS_ASSERT_EQUALS(registers[result], result_value);
              }
            }
          }
        }
      }
    }
  }


  void test__x_rshift_to() {
    SHOW_FUNC_NAME;

    for (unsigned int a_value = 0; a_value <= 0xFFFF; ++a_value) {
      clear_registers();
      p_movi(2, a_value);

      const word16_t result_value = registers[2] >> 1;

      x_rshift_to(1, 2, 3, 4);

      TS_ASSERT_EQUALS(registers[1], result_value);
    }
  }


  void test__x_rshift_8_to() {
    SHOW_FUNC_NAME;

    for (unsigned int a_value = 0; a_value <= 0xFFFF; ++a_value) {
      clear_registers();
      p_movi(2, a_value);

      const word16_t result_value = registers[2] >> 8;

      x_rshift_8_to(1, 2, 3, 4);

      TS_ASSERT_EQUALS(registers[1], result_value);
    }
  }


  void test__x_rshift_8_duo_to() {
    SHOW_FUNC_NAME;

    for (unsigned int a_value = 0; a_value <= 0xFFFF; ++a_value) {
      const unsigned int b_value = a_value + 12345;

      clear_registers();
      p_movi(2, a_value);
      p_movi(4, b_value);

      const word16_t result_a_value = registers[2] >> 8;
      const word16_t result_b_value = registers[4] >> 8;

      x_rshift_8_duo_to(1, 2, 3, 4, 5, 6, 7);

      TS_ASSERT_EQUALS(registers[1], result_a_value);
      TS_ASSERT_EQUALS(registers[3], result_b_value);
    }
  }


  void test__x_rshift_8_signed_to() {
    SHOW_FUNC_NAME;

    for (unsigned int a_value = 0; a_value <= 0xFFFF; ++a_value) {
      clear_registers();
      p_movi(2, a_value);

      const word16_t result_value = static_cast<int16_t>(registers[2]) >> 8;

      x_rshift_8_signed_to(1, 2, 3, 4, 5);

      TS_ASSERT_EQUALS(registers[1], result_value);
    }
  }


  void test__x_set0() {
    SHOW_FUNC_NAME;

    for (unsigned int result = 1; result < nb_registers; ++result) {
      clear_registers();
      p_movi(result, 666);

      TS_ASSERT_EQUALS(registers[result], 666);

      x_set0(result);

      TS_ASSERT_EQUALS(registers[result], 0);
    }
  }


  void test__x_set0x8000() {
    SHOW_FUNC_NAME;

    for (unsigned int result = 1; result < nb_registers; ++result) {
      clear_registers();
      x_set0x8000(result);

      TS_ASSERT_EQUALS(registers[result], 0x8000);
    }
  }


  void test__x_sqr() {
    SHOW_FUNC_NAME;

    clear_registers();
    clear_memory();

    p_movi(1, 0);
    p_movi(2, 0);
    x_mul_karatsuba(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0);
    TS_ASSERT_EQUALS(registers[1], 0);

    p_movi(1, 1);
    p_movi(2, 1);
    x_mul_karatsuba(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0);
    TS_ASSERT_EQUALS(registers[1], 1);

    p_movi(1, 3);
    p_movi(2, 3);
    x_mul_karatsuba(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0);
    TS_ASSERT_EQUALS(registers[1], 9);

    p_movi(1, 7);
    p_movi(2, 7);
    x_mul_karatsuba(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0);
    TS_ASSERT_EQUALS(registers[1], 0x31);

    p_movi(1, 0xF);
    p_movi(2, 0xF);
    x_mul_karatsuba(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0);
    TS_ASSERT_EQUALS(registers[1], 0xE1);

    p_movi(1, 0x1F);
    p_movi(2, 0x1F);
    x_mul_karatsuba(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0);
    TS_ASSERT_EQUALS(registers[1], 0x3C1);

    p_movi(1, 0x3F);
    p_movi(2, 0x3F);
    x_mul_karatsuba(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0);
    TS_ASSERT_EQUALS(registers[1], 0xF81);

    p_movi(1, 0x7F);
    p_movi(2, 0x7F);
    x_mul_karatsuba(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0);
    TS_ASSERT_EQUALS(registers[1], 0x3F01);

    p_movi(1, 0xFF);
    p_movi(2, 0xFF);
    x_mul_karatsuba(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0);
    TS_ASSERT_EQUALS(registers[1], 0xFE01);

    p_movi(1, 0x1FF);
    p_movi(2, 0x1FF);
    x_mul_karatsuba(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 3);
    TS_ASSERT_EQUALS(registers[1], 0xFC01);

    p_movi(1, 0x3FF);
    p_movi(2, 0x3FF);
    x_mul_karatsuba(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0xF);
    TS_ASSERT_EQUALS(registers[1], 0xF801);

    p_movi(1, 0x7FF);
    p_movi(2, 0x7FF);
    x_mul_karatsuba(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0x3F);
    TS_ASSERT_EQUALS(registers[1], 0xF001);

    p_movi(1, 0xFFF);
    p_movi(2, 0xFFF);
    x_mul_karatsuba(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0xFF);
    TS_ASSERT_EQUALS(registers[1], 0xE001);

    p_movi(1, 0x1FFF);
    p_movi(2, 0x1FFF);
    x_mul_karatsuba(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0x3FF);
    TS_ASSERT_EQUALS(registers[1], 0xC001);

    p_movi(1, 0x3FFF);
    p_movi(2, 0x3FFF);
    x_mul_karatsuba(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0xFFF);
    TS_ASSERT_EQUALS(registers[1], 0x8001);

    p_movi(1, 0x7FFF);
    p_movi(2, 0x7FFF);
    x_mul_karatsuba(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0x3FFF);
    TS_ASSERT_EQUALS(registers[1], 1);

    p_movi(1, 0xFFFF);
    p_movi(2, 0xFFFF);
    x_mul_karatsuba(1, 2, 3, 4, 5, 6, 7);
    TS_ASSERT_EQUALS(registers[2], 0xFFFE);
    TS_ASSERT_EQUALS(registers[1], 1);

    for (uint32_t a_value = 0; a_value <= 0xFFFF; ++a_value) {
      clear_registers();
      p_movi(1, a_value);
      p_movi(2, a_value);

      const uint32_t result_value = a_value*a_value;

      x_sqr(1, 2, 3, 4, 5, 6, 7);

      TS_ASSERT_EQUALS(registers[2], result_value >> 16);
      TS_ASSERT_EQUALS(registers[1], result_value & 0xFFFF);
    }
  }


  void test__x_sqr8_to() {
    SHOW_FUNC_NAME;

    for (unsigned int a_value = 0; a_value <= 0xFF; ++a_value) {
      clear_registers();
      p_movi(2, a_value);

      const word16_t result_value = a_value*a_value;

      x_sqr8_to(1, 2, 3, 4, 5);

      TS_ASSERT_EQUALS(registers[1], result_value);
    }
  }


  void test__x_sub_from() {
    SHOW_FUNC_NAME;

    const unsigned int b_value_limit = (HEAVY
                                        ? 0xFFF
                                        : 0x3F);
      
    for (unsigned int a_value = 0; a_value <= 0xFFFF; ++a_value) {
      for (unsigned int b_value = 0; b_value <= b_value_limit; ++b_value) { 
        clear_registers();
        p_movi(2, a_value);
        p_movi(3, b_value);

        const word16_t result_value = registers[2] - registers[1];

        x_sub_from(1, 2);

        TS_ASSERT_EQUALS(registers[1], result_value);
      }
    }

    for (unsigned int a_value = 1; a_value <= 0xFF; ++a_value) {
      for (unsigned int a = 1; a < nb_registers; ++a) {
        for (unsigned int b_value = 1; b_value <= b_value_limit; ++b_value) {
          for (unsigned int b = 1; b < nb_registers; ++b) {
            if (a != b) {
              clear_registers();
              p_movi(a, a_value);
              p_movi(b, b_value);

              const word16_t result_value = registers[b] - registers[a];

              x_sub_from(a, b);

              TS_ASSERT_EQUALS(registers[a], result_value);
            }
          }
        }
      }
    }
  }


  void test__x_sub_to() {
    SHOW_FUNC_NAME;

    const unsigned int b_value_limit = (HEAVY
                                        ? 0xFFF
                                        : 0x3F);
      
    for (unsigned int a_value = 0; a_value <= 0xFFFF; ++a_value) {
      for (unsigned int b_value = 0; b_value <= b_value_limit; ++b_value) { 
        clear_registers();
        p_movi(2, a_value);
        p_movi(3, b_value);

        const word16_t result_value = registers[2] - registers[3];

        x_sub_to(1, 2, 3);

        TS_ASSERT_EQUALS(registers[1], result_value);
      }
    }

    for (unsigned int result = 1; result < nb_registers; ++result) {
      for (unsigned int a_value = 1; a_value <= 0xFF; ++a_value) {
        for (unsigned int a = 1; a < nb_registers; ++a) {
          if (a == result) {
            continue;
          }

          for (unsigned int b_value = 1; b_value <= b_value_limit; ++b_value) {
            for (unsigned int b = 1; b < nb_registers; ++b) {
              if ((b == result) || (b == a)) {
                continue;
              }

              clear_registers();
              p_movi(a, a_value);
              p_movi(b, b_value);

              const word16_t result_value = registers[a] - registers[b];

              x_sub_to(result, a, b);

              TS_ASSERT_EQUALS(registers[result], result_value);
            }
          }
        }
      }
    }
  }


  void test__x_swap() {
    SHOW_FUNC_NAME;

    for (unsigned int a_value = 0; a_value <= 0xFFFF; ++a_value) {
      for (unsigned int b_value = 0; b_value <= 0x3F; ++b_value) { 
        clear_registers();
        p_movi(1, a_value);
        p_movi(2, b_value);

        x_swap(1, 2, 3);

        TS_ASSERT_EQUALS(registers[1], b_value);
        TS_ASSERT_EQUALS(registers[2], a_value);
      }
    }
  }
};
