/* -*- coding: latin-1 -*- */

/** \file benchmark.cpp (March 14, 2017)
 * \brief Simple example program to compare different multiplication algorithms.
 *
 * Piece of cpprisc16.
 * https://bitbucket.org/OPiMedia/cpprisc16
 *
 * GPLv3 --- Copyright (C) 2017 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <cmath>
#include <cstdlib>

#include <iostream>

#include "cpprisc16/cpprisc16.hpp"
#include "cpprisc16/cppextendedrisc16.hpp"

using namespace cpprisc16;


int
main() {
  const unsigned int nb = 100000;

  clear_registers();
  clear_memory();
  clear_nb_executed();
  srand(666);

  // Products
  std::cout << "x_mul: standard algorithm --- ";
  for (unsigned int i = 0; i < nb; ++i) {
    p_movi(1, rand());
    p_movi(2, rand());

    x_mul(1, 2, 3, 4, 5, 6, 7);
  }
  std::cout << nb_executed << '/' << nb << " = "
            << std::lround(static_cast<double>(nb_executed)/nb) << std::endl;
  println_all();


  clear_registers();
  clear_memory();
  clear_nb_executed();
  srand(666);

  std::cout << std::endl
            << "x_mul_karatsuba: Karatsuba algorithm --- ";
  for (unsigned int i = 0; i < nb; ++i) {
    p_movi(1, rand());
    p_movi(2, rand());

    x_mul_karatsuba(1, 2, 3, 4, 5, 6, 7);
  }
  std::cout << nb_executed << '/' << nb << " = "
            << std::lround(static_cast<double>(nb_executed)/nb) << std::endl;
  println_all();


  // Squares
  clear_registers();
  clear_memory();
  clear_nb_executed();
  srand(666);

  std::cout << std::endl
            << "=== Squares ===" << std::endl
            << "x_mul: standard algorithm --- ";
  for (unsigned int i = 0; i < nb; ++i) {
    p_movi(1, rand());
    p_movi(2, registers[1]);

    x_mul(1, 2, 3, 4, 5, 6, 7);
  }
  std::cout << nb_executed << '/' << nb << " = "
            << std::lround(static_cast<double>(nb_executed)/nb) << std::endl;
  println_all();


  clear_registers();
  clear_memory();
  clear_nb_executed();
  srand(666);

  std::cout << std::endl
            << "x_sqr --- ";
  for (unsigned int i = 0; i < nb; ++i) {
    p_movi(1, rand());
    p_movi(2, registers[1]);

    x_sqr(1, 2, 3, 4, 5, 6, 7);
  }
  std::cout << nb_executed << '/' << nb << " = "
            << std::lround(static_cast<double>(nb_executed)/nb) << std::endl;
  println_all();

  return EXIT_SUCCESS;
}
