.. -*- restructuredtext -*-

=========
cpprisc16
=========
C++ library to easily write and test "assembly" code for RiSC-16 (Ridiculously Simple Computer).

(See `The RiSC-16 Architecture`_ by Bruce Jacob.)

.. _`The RiSC-16 Architecture`: https://www.ece.umd.edu/~blj/RiSC/


* This complete C++ sources on Bitbucket_
* The `C++ HTML online documentation`_

    * `cpprisc16.hpp`_ — Instructions set of **RiSC16**: 8 instructions i_* and 4 pseudo-instructions p_*.
    * `cppextendedrisc16.hpp`_ — Extended instructions set: some **extra operations** x_* implemented with RiSC16.
    * `cppis1.hpp`_ — Additional **Instructions Set 1 IS[1]**: 8 new instructions is1_* and 1 instruction modified is1_add.
    * `cppis2.hpp`_ — Additional **Instructions Set 2 IS[2]**: 8 new instructions is2_* and 1 instruction modified is2_add.

.. _Bitbucket: https://bitbucket.org/OPiMedia/cpprisc16/
.. _`C++ HTML online documentation`: http://www.opimedia.be/DS/online-documentations/cpprisc16/html/
.. _`cpprisc16.hpp`: http://www.opimedia.be/DS/online-documentations/cpprisc16/html/cpprisc16_8hpp.html
.. _`cppextendedrisc16.hpp`: http://www.opimedia.be/DS/online-documentations/cpprisc16/html/cppextendedrisc16_8hpp.html
.. _`cppis1.hpp`: http://www.opimedia.be/DS/online-documentations/cpprisc16/html/cppis1_8hpp.html
.. _`cppis2.hpp`: http://www.opimedia.be/DS/online-documentations/cpprisc16/html/cppis2_8hpp.html



Very simple example: swap values of two registers.
==================================================
Instead this strict RiSC16 assembler code:

.. code-block::

    movi 1, 0x42
    movi 2, 0x666

    // r1, r2 <-- r2, r1   (change r3)
    add 3, 1, 0
    add 1, 2, 0
    add 2, 3, 0

    halt


You can write like this in a C++ program:

.. code-block:: C++

  p_movi(1, 0x42);
  p_movi(2, 0x666);

  // It is possible to use any C++ code and to print register(s)
  std::cout << "Before swapping" << std::endl;
  println_reg(1);
  println_reg(2);

  // r1, r2 <-- r2, r1   (change r3)
  i_add(3, 1, 0);
  i_add(1, 2, 0);
  i_add(2, 3, 0);

  // Print # executed instructions, all registers and memory (if used) before end
  p_halt();


Of course it is possible to add any C++ code.

There are also a lot of additional instructions, and some useful `printing functions`_.

.. _`printing functions`: http://www.opimedia.be/DS/online-documentations/cpprisc16/html/cpprisc16_8hpp.html
