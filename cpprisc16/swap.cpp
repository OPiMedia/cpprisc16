/* -*- coding: latin-1 -*- */

/** \file swap.cpp (March 15, 2017)
 * \brief Very simple example program.
 *
 * Piece of cpprisc16.
 * https://bitbucket.org/OPiMedia/cpprisc16
 *
 * Simple example: swap values of two registers.
 *
 * GPLv3 --- Copyright (C) 2017 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <cstdlib>

#include <iostream>

#include "cpprisc16/cpprisc16.hpp"
#include "cpprisc16/cppextendedrisc16.hpp"

using namespace cpprisc16;


int
main() {
  clear_registers();
  clear_memory();

  /*
    Equivalent to this assembler program:
    movi 1, 0x42
    movi 2, 0x666

    // r1, r2 <-- r2, r1   (change r3)
    add 3, 1, 0
    add 1, 2, 0
    add 2, 3, 0
   */

  p_movi(1, 0x42);
  p_movi(2, 0x666);

  // It is possible to use any C++ code and to print register(s)
  std::cout << "Before swapping" << std::endl;
  println_reg(1);
  println_reg(2);

  // With only RiSC16 instructions
  clear_nb_executed();
  i_add(3, 1, 0);
  i_add(1, 2, 0);
  i_add(2, 3, 0);

  // Print # executed instructions, registers and memory (if used)
  println_all();


  // With extended instruction x_swap
  clear_nb_executed();

  x_swap(1, 2, 3);

  // Print # executed instructions,  registers and memory (if used) before end
  p_halt();

  return EXIT_SUCCESS;
}
