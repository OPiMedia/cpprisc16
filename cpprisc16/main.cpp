/* -*- coding: latin-1 -*- */

/** \file main.cpp (March 14, 2017)
 * \brief Simple example program.
 *
 * Piece of cpprisc16.
 * https://bitbucket.org/OPiMedia/cpprisc16
 *
 * GPLv3 --- Copyright (C) 2017 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <cstdlib>

#include "cpprisc16/cpprisc16.hpp"
#include "cpprisc16/cppextendedrisc16.hpp"

using namespace cpprisc16;


int
main() {
  clear_registers();
  clear_memory();

  const word16_t a = 0x7FFF;
  const word16_t b = 0x010F;

  p_movi(1, a);
  p_movi(2, b);

  clear_nb_executed();

  x_mul(1, 2, 3, 4, 5, 6, 7);
  println_all();


  clear_registers();
  clear_memory();

  p_movi(1, a);
  p_movi(2, b);

  clear_nb_executed();

  x_mul_karatsuba(1, 2, 3, 4, 5, 6, 7);
  p_halt();

  return EXIT_SUCCESS;
}
