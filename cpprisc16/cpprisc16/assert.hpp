/* -*- coding: latin-1 -*- */

/** \file cpprisc16/assert.hpp (March 12, 2017)
 * \brief Helper assertions.
 *
 * Piece of cpprisc16.
 * https://bitbucket.org/OPiMedia/cpprisc16
 *
 * GPLv3 --- Copyright (C) 2017 Olivier Pirson
 * http://www.opimedia.be/
 */

#ifndef CPPRISC16_CPPRISC16_ASSERT_HPP_
#define CPPRISC16_CPPRISC16_ASSERT_HPP_

#include <cstdint>


/* ********
 * Macros *
 **********/

/** \brief
 * Check if a, b and c are different
 */
#define ASSERT_3_DIFFERENT(a, b, c) {           \
    assert(a != b);                             \
    assert(a != c);                             \
                                                \
    assert(b != c);                             \
  }


/** \brief
 * Check if a, b, c and d are different
 */
#define ASSERT_4_DIFFERENT(a, b, c, d) {        \
    assert(a != b);                             \
    assert(a != c);                             \
    assert(a != d);                             \
                                                \
    assert(b != c);                             \
    assert(b != d);                             \
                                                \
    assert(c != d);                             \
  }


/** \brief
 * Check if a, b, c, d and e are different
 */
#define ASSERT_5_DIFFERENT(a, b, c, d, e) {     \
    assert(a != b);                             \
    assert(a != c);                             \
    assert(a != d);                             \
    assert(a != e);                             \
                                                \
    assert(b != c);                             \
    assert(b != d);                             \
    assert(b != e);                             \
                                                \
    assert(c != d);                             \
    assert(c != e);                             \
                                                \
    assert(d != e);                             \
  }


/** \brief
 * Check if a, b, c, d, e and f are different
 */
#define ASSERT_6_DIFFERENT(a, b, c, d, e, f) {       \
    assert(a != b);                                  \
    assert(a != c);                                  \
    assert(a != d);                                  \
    assert(a != e);                                  \
    assert(a != f);                                  \
                                                     \
    assert(b != c);                                  \
    assert(b != d);                                  \
    assert(b != e);                                  \
    assert(b != f);                                  \
                                                     \
    assert(c != d);                                  \
    assert(c != e);                                  \
    assert(c != f);                                  \
                                                     \
    assert(d != e);                                  \
    assert(d != f);                                  \
                                                     \
    assert(e != f);                                  \
  }


/** \brief
 * Check if a, b, c, d, e, f and g are different
 */
#define ASSERT_7_DIFFERENT(a, b, c, d, e, f, g) {       \
    assert(a != b);                                     \
    assert(a != c);                                     \
    assert(a != d);                                     \
    assert(a != e);                                     \
    assert(a != f);                                     \
    assert(a != g);                                     \
                                                        \
    assert(b != c);                                     \
    assert(b != d);                                     \
    assert(b != e);                                     \
    assert(b != f);                                     \
    assert(b != g);                                     \
                                                        \
    assert(c != d);                                     \
    assert(c != e);                                     \
    assert(c != f);                                     \
    assert(c != g);                                     \
                                                        \
    assert(d != e);                                     \
    assert(d != f);                                     \
    assert(d != g);                                     \
                                                        \
    assert(e != f);                                     \
    assert(e != g);                                     \
                                                        \
    assert(f != g);                                     \
  }

#endif  // CPPRISC16_CPPRISC16_ASSERT_HPP_
