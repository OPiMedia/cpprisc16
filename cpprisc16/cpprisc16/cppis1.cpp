/* -*- coding: latin-1 -*- */

/** \file cpprisc16/cppis1.cpp (March 14, 2017)
 * \brief Additional Instructions Set 1 IS[1].
 *
 * Piece of cpprisc16.
 * https://bitbucket.org/OPiMedia/cpprisc16
 *
 * GPLv3 --- Copyright (C) 2017 Olivier Pirson
 * http://www.opimedia.be/
 */

#include "cppis1.hpp"


namespace cpprisc16 {

  /* **********************************
   * Functions for IS[1] instructions *
   ************************************/
  bool
  is1_add(unsigned int result, unsigned int a, unsigned int b) {
    assert(result < nb_registers);
    assert(a < nb_registers);
    assert(b < nb_registers);

    assert(sizeof(unsigned int) > sizeof(word16_t));

    const std::uint32_t sum = (static_cast<std::uint32_t>(registers[a])
                               + static_cast<std::uint32_t>(registers[b]));

    registers[result] = sum;
    registers[0] = 0;

    ++nb_executed;

    return (sum >> 16) != 0;
  }


  void
  is1_nor(unsigned int result, unsigned int a, unsigned int b) {
    assert(result < nb_registers);
    assert(a < nb_registers);
    assert(b < nb_registers);

    registers[result] = ~(registers[a] | registers[b]);
    registers[0] = 0;

    ++nb_executed;
  }


  bool
  is1_sha(unsigned int result, unsigned int a, unsigned int b) {
    assert(result < nb_registers);
    assert(a < nb_registers);
    assert(b < nb_registers);

    ++nb_executed;

    const bool left = (static_cast<std::int16_t>(registers[b]) >= 0);
    const word16_t nb = (left
                         ? registers[b]
                         : -registers[b]);

    if (nb < 16) {
      const bool overflow
        = (left
           ? registers[a] >> (16 - nb)
           : static_cast<std::uint16_t>(registers[a] << (16 - nb))) != 0;
      //  lost piece != 0

      registers[result] = (left
                           ? registers[a] << nb
                           : static_cast<std::int16_t>(registers[a]) >> nb);
      registers[0] = 0;

      return overflow;
    }
    else {
      // Shift too big for C++ shift operator
      const bool overflow = (registers[a] != 0);

      registers[result] = 0;

      return overflow;
    }
  }


  bool
  is1_shl(unsigned int result, unsigned int a, unsigned int b) {
    assert(result < nb_registers);
    assert(a < nb_registers);
    assert(b < nb_registers);

    ++nb_executed;

    const bool left = (static_cast<std::int16_t>(registers[b]) >= 0);
    const word16_t nb = (left
                         ? registers[b]
                         : -registers[b]);

    if (nb < 16) {
      const bool overflow
        = (left
           ? registers[a] >> (16 - nb)
           : static_cast<std::uint16_t>(registers[a] << (16 - nb))) != 0;
      //  lost piece != 0

      registers[result] = (left
                           ? registers[a] << nb
                           : registers[a] >> nb);
      registers[0] = 0;

      return overflow;
    }
    else {
      // Shift too big for C++ shift operator
      const bool overflow = (registers[a] != 0);

      registers[result] = 0;

      return overflow;
    }
  }


  void
  is1_shifti(unsigned int result, unsigned int a, immed_t immed7) {
    assert(result < nb_registers);
    assert(a < nb_registers);
    assert(immed7 <= 0x7F);

    //                          6 5 43210
    // immed7 is decomposed in  x:M:immed5

    const bool arithmetic = immed7 & 0x20;  // bit 5
    const bool right = immed7 & 0x10;  // bit 4 == sign of immed5

    if (right) {  // negative value --> absolute value for first 5 bits
      immed7 = -(immed7 | 0xFFF0);
    }

    immed7 &= 0x1F;  // keep only 5 first bits

    assert(immed7 < 16);

    if (immed7 < 16) {
      registers[result]
        = (right
           ? (arithmetic
              ? static_cast<std::int16_t>(registers[a]) >> immed7
              : registers[a] >> immed7)
           : registers[a] << immed7);

      registers[0] = 0;
    }
    else {
      // Shift too big for C++ shift operator
      registers[result] = 0;
    }

    ++nb_executed;
  }


  bool
  is1_sub(unsigned int result, unsigned int a, unsigned int b) {
    assert(result < nb_registers);
    assert(a < nb_registers);
    assert(b < nb_registers);

    const bool overflow = (registers[a] < registers[b]);
    const unsigned int diff = registers[a] - registers[b];

    registers[result] = diff;
    registers[0] = 0;

    ++nb_executed;

    return overflow;
  }


  void
  is1_xor(unsigned int result, unsigned int a, unsigned int b) {
    assert(result < nb_registers);
    assert(a < nb_registers);
    assert(b < nb_registers);

    registers[result] = registers[a] ^ registers[b];
    registers[0] = 0;

    ++nb_executed;
  }

}  // namespace cpprisc16
