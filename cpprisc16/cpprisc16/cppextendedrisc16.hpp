/* -*- coding: latin-1 -*- */

/** \file cpprisc16/cppextendedrisc16.hpp (March 15, 2017)
 * \brief Extended instructions set:
 * some extra operations x_* implemented with RiSC16.
 *
 * Piece of cpprisc16.
 * https://bitbucket.org/OPiMedia/cpprisc16
 *
 * GPLv3 --- Copyright (C) 2017 Olivier Pirson
 * http://www.opimedia.be/
 */

#ifndef CPPRISC16_CPPRISC16_CPPEXTENDEDRISC16_HPP_
#define CPPRISC16_CPPRISC16_CPPEXTENDEDRISC16_HPP_

#include "cpprisc16.hpp"


/* *******
 * Macro *
 *********/
/** \brief
 * Jump to label.
 */
#define x_branch(label) {                       \
    ++cpprisc16::nb_executed;                   \
    goto label;                                 \
  }



namespace cpprisc16 {

  /* ****************************************************
   * Prototypes for RiSC16 extended pseudo-instructions *
   ******************************************************/

  /** \brief
   * R[a2]:R[a1] <-- R[a2]:R[a1] + R[b2]:R[b1]
   *
   * a2, a1, b2, b1, tmp1, tmp2, tmp3 must be different
   */
  void
  x_add32(unsigned int a2, unsigned int a1, unsigned int b2, unsigned int b1,
          unsigned int tmp1, unsigned int tmp2, unsigned int tmp3);


  /** \brief
   * R[b]:R[a] <-- R[a] + R[b]
   *
   * a, b, tmp1, tmp2, tmp3 must be different
   */
  void
  x_addc(unsigned int a, unsigned int b,
         unsigned int tmp1, unsigned int tmp2, unsigned int tmp3);


  /** \brief
   * R[result] <-- R[a] & R[b]
   */
  void
  x_and_to(unsigned int result, unsigned int a, unsigned int b);


  /** \brief
   * R[a2]:R[a1] <-- R[a2]:R[a1] + 1
   *
   * a2, a1 must be different
   */
  void
  x_inc32(unsigned int a2, unsigned int a1);


  /** \brief
   * R[result] <-- (positive value) if a < b,
   *                0                else
   */
  void
  x_is_lower_to(unsigned int result, unsigned int a, unsigned int b,
                unsigned int tmp);


  /** \brief
   * R[a] <-- R[a] << 1 (== R[a]*2)
   */
  void
  x_lshift(unsigned int a);


  /** \brief
   * R[result] <-- R[a] << 1 (== R[a]*2)
   */
  void
  x_lshift_to(unsigned int result, unsigned int a);


  /** \brief
   * R[a2]:R[a1] <-- (R[a2]:R[a1]) << 1 (== (R[a2]:R[a1])*2)
   *
   * a1, a2, tmp must be different
   */
  void
  x_lshift32(unsigned int a2, unsigned int a1,
             unsigned int tmp);


  /** \brief
   * R[a] <-- R[a] << 8 (== R[a]*256)
   */
  void
  x_lshift_8(unsigned int a);


  /** \brief
   * R[result] <-- R[a] << 8 (== R[a]*256)
   */
  void
  x_lshift_8_to(unsigned int result, unsigned int a);


  /** \brief
   * R[a2]:R[a1] <-- (R[a2]:R[a1]) << 8 (== (R[a2]:R[a1])*256)
   *
   * a1, a2, tmp1, tmp2, tmp3 must be different
   */
  void
  x_lshift32_8(unsigned int a2, unsigned int a1,
               unsigned int tmp1, unsigned int tmp2, unsigned int tmp3);


  /** \brief
   * R[a] <-- R[a] & 0x8000 (== R[a] & 0b1000000000000000 == R[a] & 32768)
   *
   * If (MSB of R[a]) == 1
   * then the result is 0x8000,
   * else the result is 0.
   *
   * @param a != tmp
   * @param tmp
   */
  void
  x_mask0x8000(unsigned int a,
               unsigned int tmp);


  /** \brief
   * R[result] <-- R[a] & 0x8000 (== R[a] & 0b1000000000000000 == R[a] & 32768)
   *
   * If (MSB of R[a]) == 1
   * then the result is 0x8000,
   * else the result is 0.
   *
   * result, a must be different
   */
  void
  x_mask0x8000_to(unsigned int result, unsigned int a);


  /** \brief
   * R[result] <-- R[a]
   */
  void
  x_mov(unsigned int result, unsigned int a);


  /** \brief
   * R[b]:R[a] <-- R[a] * R[b]
   * by standard algorithm:
   * https://en.wikipedia.org/wiki/Multiplication_algorithm#Long_multiplication
   *
   * a, b, tmp1, tmp2, tmp3, tmp4, tmp5 must be different
   *
   * Side effect: Use memory 0x0 to 0x3 (included) to save temporary work.
   */
  void
  x_mul(unsigned int a, unsigned int b,
        unsigned int tmp1, unsigned int tmp2, unsigned int tmp3,
        unsigned int tmp4, unsigned int tmp5);


  /** \brief
   * R[b]:R[a] <-- R[a] * R[b]
   * by Karatsuba algorithm:
   * https://en.wikipedia.org/wiki/Karatsuba_algorithm
   *
   * a, b, tmp1, tmp2, tmp3, tmp4, tmp5 must be different
   *
   * Side effect: Use memory 0x0 to 0x5 (included) to save temporary work.
   */
  void
  x_mul_karatsuba(unsigned int a, unsigned int b,
                  unsigned int tmp1, unsigned int tmp2, unsigned int tmp3,
                  unsigned int tmp4, unsigned int tmp5);


  /** \brief
   * R[result] <-- R[a] * R[b]
   *
   * result, a, b, tmp1, tmp2 must be different
   *
   * @param result
   * @param a R[a] <= 0xFF (== 0b11111111 == 255)
   * @param b R[b] <= 0xFF
   * @param tmp1
   * @param tmp2
   */
  void
  x_mul8_to(unsigned int result, unsigned int a, unsigned int b,
            unsigned int tmp1, unsigned int tmp2);


  /** \brief
   * R[a] <-- -R[a] (two's complement)
   */
  void
  x_neg(unsigned int a);


  /** \brief
   * R[a] <-- ~R[a]
   */
  void
  x_not(unsigned int a);


  /** \brief
   * R[result] <-- ~R[a]
   */
  void
  x_not_to(unsigned int result, unsigned int a);


  /** \brief
   * R[result] <-- R[a] | R[b]
   *
   * @param result
   * @param a != 0 and != b
   * @param b != 0
   */
  void
  x_or_to(unsigned int result, unsigned int a, unsigned int b);


  /** \brief
   * R[result] <-- R[a] >> 1 (== R[a]/2)
   *
   * result, a, tmp1, tmp2 must be different
   *
   * Side effect: change R[a], R[tmp1], R[tmp2]
   */
  void
  x_rshift_to(unsigned int result, unsigned int a,
              unsigned int tmp1, unsigned int tmp2);


  /** \brief
   * R[result] <-- R[a] >> 8 (== R[a]/256)
   *
   * result, a, tmp1, tmp2 must be different
   *
   * Side effect: change R[tmp1], R[tmp2]
   */
  void
  x_rshift_8_to(unsigned int result, unsigned int a,
                unsigned int tmp1, unsigned int tmp2);


  /** \brief
   * R[resulta] <-- R[a] >> 8 (== R[a]/256)
   * R[resultb] <-- R[b] >> 8
   *
   * resulta, a, resultb, b, tmp1, tmp2, tmp3 must be different
   *
   * Side effect: change R[tmp1], R[tmp2], R[tmp3]
   */
  void
  x_rshift_8_duo_to(unsigned int resulta, unsigned int a,
                    unsigned int resultb, unsigned int b,
                    unsigned int tmp1, unsigned int tmp2, unsigned int tmp3);


  /** \brief
   * R[result] <-- R[a] >> 8 with extension of the sign
   *
   * result, a, tmp1, tmp2, tmp3 must be different
   *
   * Side effect: change R[tmp1], R[tmp2], R[tmp3]
   */
  void
  x_rshift_8_signed_to(unsigned int result, unsigned int a,
                       unsigned int tmp1, unsigned int tmp2,
                       unsigned int tmp3);


  /** \brief
   * R[a] <-- 0
   */
  void
  x_set0(unsigned int a);


  /** \brief
   * R[result] <-- 0x8000 (== 0b1000000000000000 == 32768)
   */
  void
  x_set0x8000(unsigned int result);


  /** \brief
   * R[result2]:R[a] <-- R[a]*R[a]
   *
   * a, result2, tmp1, tmp2, tmp3, tmp4, tmp5 must be different
   *
   * Side effect: Use memory 0x0 to 0x1 (included) to save temporary work.
   */
  void
  x_sqr(unsigned int a, unsigned int result2,
        unsigned int tmp1, unsigned int tmp2, unsigned int tmp3,
        unsigned int tmp4, unsigned int tmp5);


  /** \brief
   * R[result] <-- R[a] * R[a]
   *
   * result, a, tmp1, tmp2, tmp3 must be different
   *
   * @param result
   * @param a R[a] <= 0xFF (== 0b11111111 == 255)
   * @param tmp1
   * @param tmp2
   * @param tmp3
   */
  void
  x_sqr8_to(unsigned int result, unsigned int a,
            unsigned int tmp1, unsigned int tmp2, unsigned int tmp3);


  /** \brief
   * R[a] <-- -R[a] + R[b]
   */
  void
  x_sub_from(unsigned int a, unsigned int b);


  /** \brief
   * R[result] <-- R[a] - R[b]
   */
  void
  x_sub_to(unsigned int result, unsigned int a, unsigned int b);


  /** \brief
   * R[a], R[b] <-- R[b], R[a]
   */
  void
  x_swap(unsigned int a, unsigned int b, unsigned int tmp);

}  // namespace cpprisc16


#endif  // CPPRISC16_CPPRISC16_CPPEXTENDEDRISC16_HPP_
