/* -*- coding: latin-1 -*- */

/** \file cpprisc16/cpprisc16.cpp (March 15, 2017)
 * \brief Instructions set of RiSC16.
 *
 * Piece of cpprisc16.
 * https://bitbucket.org/OPiMedia/cpprisc16
 *
 * GPLv3 --- Copyright (C) 2017 Olivier Pirson
 * http://www.opimedia.be/
 */

#include "cpprisc16.hpp"

#include <cstdlib>

#include <algorithm>
#include <bitset>
#include <iomanip>
#include <ios>
#include <iostream>


namespace cpprisc16 {
  /* ******************
   * Global constants *
   ********************/
  const unsigned int nb_registers = 8;

  const unsigned int size_memory = 256;



  /* ******************
   * Global variables *
   ********************/
  unsigned int after_last_memory_acceded = 0;

  word16_t memory[256];

  uint64_t nb_executed = 0;

  word16_t registers[8] = {0};



  /* ***********************************
   * Functions for RiSC16 instructions *
   *************************************/
  void
  i_add(unsigned int result, unsigned int a, unsigned int b) {
    assert(result < nb_registers);
    assert(a < nb_registers);
    assert(b < nb_registers);

    registers[result] = registers[a] + registers[b];
    registers[0] = 0;

    ++nb_executed;
  }


  void
  i_addi(unsigned int result, unsigned int a, immed_t immed6) {
    assert(result < nb_registers);
    assert(a < nb_registers);
    assert(immed6 <= 0x3F);

    registers[result] = registers[a] + immed6;
    registers[0] = 0;

    ++nb_executed;
  }


  void
  i_jalr(unsigned int result, unsigned int a) {
    assert(result < nb_registers);
    assert(a < nb_registers);

    std::cout << "i_jalr() and p_reset() are NOT implemented!";
    println_all();

    ++nb_executed;

    exit(1);
  }


  void
  i_lui(unsigned int result, immed_t immed10) {
    assert(result < nb_registers);
    assert(immed10 <= 0x3FF);

    registers[result] = (immed10 << 6);
    registers[0] = 0;

    ++nb_executed;
  }


  void
  i_lw(unsigned int result, unsigned int a, immed_t immed6) {
    assert(result < nb_registers);
    assert(a < nb_registers);
    assert(immed6 <= 0x3F);

    const unsigned int i = registers[a] + immed6;

    assert(i < size_memory);

    registers[result] = memory[i];
    after_last_memory_acceded = std::max(after_last_memory_acceded, i + 1);

    ++nb_executed;
  }


  void
  i_nand(unsigned int result, unsigned int a, unsigned int b) {
    assert(result < nb_registers);
    assert(a < nb_registers);
    assert(b < nb_registers);

    registers[result] = ~(registers[a] & registers[b]);
    registers[0] = 0;

    ++nb_executed;
  }


  void
  i_sw(unsigned int a, unsigned int result, immed_t immed6) {
    assert(result < nb_registers);
    assert(a < nb_registers);
    assert(immed6 <= 0x3F);

    const unsigned int i = registers[result] + immed6;

    assert(i < size_memory);

    memory[i] = registers[a];
    after_last_memory_acceded = std::max(after_last_memory_acceded, i + 1);

    ++nb_executed;
  }



  /* ******************************************
   * Functions for RiSC16 pseudo-instructions *
   ********************************************/
  void
  p_halt(bool print) {
    ++nb_executed;

    if (print) {
      println_all();
    }

    exit(EXIT_SUCCESS);
  }


  void
  p_movi(unsigned int result, immed_t immed) {
    assert(result < nb_registers);

    i_lui(result, immed >> 6);
    i_addi(result, result, immed & 0x3F);
  }


  void
  p_nop() {
    i_add(0, 0, 0);
  }


  void
  p_reset() {
    i_jalr(0, 0);  // NOT implemented!
  }



  /* *******************
   * General functions *
   *********************/
  void
  clear_memory() {
    for (unsigned int i = 0; i < size_memory; ++i) {
      memory[i] = 0;
    }
    after_last_memory_acceded = 0;
  }


  void
  clear_nb_executed() {
    nb_executed = 0;
  }


  void
  clear_registers() {
    // registers[0] must be always 0
    for (unsigned int i = 1; i < nb_registers; ++i) {
      registers[i] = 0;
    }
  }


  void
  print_mem(unsigned int i) {
    std::cout << "M[" << i << "] = ";
    print_value16(memory[i]);
  }
  void
  print_reg(unsigned int a) {
    assert(a < nb_registers);

    std::cout << "R[" << a << "] = ";
    print_value16(registers[a]);
  }


  void
  print_reg2(unsigned int a2, unsigned int a1) {
    assert(a2 < nb_registers);
    assert(a1 < nb_registers);

    std::cout << "R[" << a2 << ':' << a1 << "] = ";
    print_value32((static_cast<uint32_t>(registers[a2]) << 16)
                  | registers[a1]);
  }


  void
  print_value16(std::uint16_t n) {
    std::cout << "0x" << std::setw(4) << std::hex << n
              << " = 0b" << std::bitset<16>(n)
              << " = " << std::setw(5) << std::dec << n
              << " = " << std::setw(6) << std::dec
              << static_cast<std::int16_t>(n);
  }


  void
  print_value32(std::uint32_t n) {
    std::cout << "0x" << std::setw(8) << std::hex << n
              << " = 0b" << std::bitset<32>(n)
              << " = " << std::setw(9) << std::dec << n
              << " = " << std::setw(10) << std::dec
              << static_cast<std::int32_t>(n);
  }


  void
  println_all() {
    println_infos();
    println_registers();
    if (after_last_memory_acceded > 0) {
      std::cout << std::endl;
    }
    println_memory();
  }


  void
  println_infos() {
    std::cout << "# instructions executed = " << nb_executed << std::endl;
  }


  void
  println_mem(unsigned int i) {
    print_mem(i);
    std::cout << std::endl;
  }


  void
  println_memory(unsigned int size) {
    for (unsigned int i = 0; i < (size == 0
                                  ? after_last_memory_acceded
                                  : size); ++i) {
      println_mem(i);
    }
  }


  void
  println_reg(unsigned int a) {
    assert(a < nb_registers);

    print_reg(a);
    std::cout << std::endl;
  }


  void
  println_reg2(unsigned int a2, unsigned int a1) {
    assert(a2 < nb_registers);
    assert(a1 < nb_registers);

    print_reg2(a2, a1);
    std::cout << std::endl;
  }


  void
  println_registers() {
    for (unsigned int i = 0; i < nb_registers; ++i) {
      println_reg(i);
    }
  }


  void
  println_value16(std::uint16_t n) {
    print_value16(n);
    std::cout << std::endl;
  }


  void
  println_value32(std::uint32_t n) {
    print_value32(n);
    std::cout << std::endl;
  }

}  // namespace cpprisc16
