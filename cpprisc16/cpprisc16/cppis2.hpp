/* -*- coding: latin-1 -*- */

/** \file cpprisc16/cppis2.hpp (March 15, 2017)
 * \brief Additional Instructions Set 2 IS[2]:
 * 8 new instructions is2_* and 1 instruction modified is2_add.
 *
 * Piece of cpprisc16.
 * https://bitbucket.org/OPiMedia/cpprisc16
 *
 * GPLv3 --- Copyright (C) 2017 Olivier Pirson
 * http://www.opimedia.be/
 */

#ifndef CPPRISC16_CPPRISC16_CPPIS2_HPP_
#define CPPRISC16_CPPRISC16_CPPIS2_HPP_

#include "cpprisc16.hpp"


/* *******************************
 * Macros for IS[2] instructions *
 *********************************/

/** \brief (Add Branch if Overflow)
 * R[result] <-- R[a] + R[b]
 * and if overflow
 * then jump to label.
 *
 * Count for 1 instruction.
 */
#define is2_add_bo(result, a, b, label) {       \
    assert(a < cpprisc16::nb_registers);        \
    assert(b < cpprisc16::nb_registers);        \
                                                \
    if (is2_add(result, a, b)) { goto label; }  \
  }


/** \brief (Branch if Lower)
 * If R[a] < R[b]
 * then jump to label.
 *
 * Count for 1 instruction.
 */
#define is2_bl(a, b, label) {                                           \
    assert(a < cpprisc16::nb_registers);                                \
    assert(b < cpprisc16::nb_registers);                                \
                                                                        \
    ++cpprisc16::nb_executed;                                           \
    if (cpprisc16::registers[a] < cpprisc16::registers[b]) { goto label; } \
  }


/** \brief (SHift Arithmetic)
 * R[result] <-- (R[a] << R[b]) or (R[a] >> -R[b])
 *
 * If R[b] >= 0
 * then shift to the left,
 * else shift to the right with *duplication of the sign bit*.
 *
 * And if overflow
 * then jump to label.
 *
 * Count for 1 instruction.
 *
 * @return true iff overflow
 */
#define is2_sha_bo(result, a, b, label) {       \
    assert(a < cpprisc16::nb_registers);        \
    assert(b < cpprisc16::nb_registers);        \
                                                \
    if (is2_sha(result, a, b)) { goto label; }  \
  }


/** \brief (SHift Logic if Overflow)
 * R[result] <-- (R[a] << R[b]) or (R[a] >> -R[b])
 *
 * If R[b] >= 0
 * then shift to the left,
 * else shift to the right.
 *
 * And if overflow
 * then jump to label.
 *
 * Count for 1 instruction.
 */
#define is2_shl_bo(result, a, b, label) {       \
    assert(a < cpprisc16::nb_registers);        \
    assert(b < cpprisc16::nb_registers);        \
                                                \
    if (is2_shl(result, a, b)) { goto label; }  \
  }


/** \brief (Sub Branch if Overflow)
 * R[result] <-- R[a] - R[b]
 * and if overflow
 * then jump to label.
 *
 * Count for 1 instruction.
 */
#define is2_sub_bo(result, a, b, label) {       \
    assert(a < cpprisc16::nb_registers);        \
    assert(b < cpprisc16::nb_registers);        \
                                                \
    if (is2_sub(result, a, b)) { goto label; }  \
  }



namespace cpprisc16 {

  /* ***********************************
   * Prototypes for IS[2] instructions *
   *************************************/
  /** \brief
   * R[result] <-- R[a] + R[b]
   *
   * See macro is2_add_bo() for jump if overflow.
   *
   * Count for 1 instruction.
   *
   * @return true iff overflow
   */
  bool
  is2_add(unsigned int result, unsigned int a, unsigned int b);


  /** \brief
   * R[result - 1]:R[result] <-- R[a]*R[b]
   *
   * Count for 1 instruction.
   *
   * @param result >= 1
   * @param a
   * @param b
   */
  void
  is2_mul(unsigned int result, unsigned int a, unsigned int b);


  /** \brief
   * R[result] <-- R[a] NOR R[b] (== ~(a | b))
   *
   * Count for 1 instruction.
   */
  void
  is2_nor(unsigned int result, unsigned int a, unsigned int b);


  /** \brief (SHift Arithmetic)
   * R[result] <-- (R[a] << R[b]) or (R[a] >> -R[b])
   *
   * If R[b] >= 0
   * then shift to the left,
   * else shift to the right with *duplication of the sign bit*.
   *
   * See macro is2_sha_bo() for jump if overflow.
   *
   * Count for 1 instruction.
   *
   * @return true iff overflow
   */
  bool
  is2_sha(unsigned int result, unsigned int a, unsigned int b);


  /** \brief (Shift Immediate)
   * R[result] <-- (R[a] << immed5) or (R[a] >> -immed5)
   *
   *                          6 5 43210
   * immed7 is decomposed in  x:M:immed5
   * where: x is 1 bit unused
   *        M is 1 bit: 0 for logic mode, 1 for arithmetic mode
   *        immed5 is 5 signed bits
   *
   * If immed5 >= 0
   * then shift to the left,
   * else shift to the right
   *      (with *duplication of the sign bit* if arithmetic mode).
   *
   * Count for 1 instruction.
   */
  void
  is2_shifti(unsigned int result, unsigned int a, immed_t immed7);


  /** \brief (SHift Logic)
   * R[result] <-- (R[a] << R[b]) or (R[a] >> -R[b])
   *
   * If R[b] >= 0
   * then shift to the left,
   * else shift to the right.
   *
   * See macro is2_shl_bo() for jump if overflow.
   *
   * Count for 1 instruction.
   *
   * @return true iff overflow
   */
  bool
  is2_shl(unsigned int result, unsigned int a, unsigned int b);


  /** \brief
   * R[result] <-- R[a] - R[b]
   *
   * See macro is2_sub_bo() for jump if overflow.
   *
   * Count for 1 instruction.
   *
   * @return true iff overflow
   */
  bool
  is2_sub(unsigned int result, unsigned int a, unsigned int b);


  /** \brief
   * R[result] <-- R[a] XOR R[b] (== ~(a ^ b))
   *
   * Count for 1 instruction.
   */
  void
  is2_xor(unsigned int result, unsigned int a, unsigned int b);

}  // namespace cpprisc16


#endif  // CPPRISC16_CPPRISC16_CPPIS2_HPP_
