/* -*- coding: latin-1 -*- */

/** \file cpprisc16/cppis2.cpp (March 14, 2017)
 * \brief Additional Instructions Set 2 IS[2].
 *
 * Piece of cpprisc16.
 * https://bitbucket.org/OPiMedia/cpprisc16
 *
 * GPLv3 --- Copyright (C) 2017 Olivier Pirson
 * http://www.opimedia.be/
 */

#include "cppis2.hpp"

#include "cppis1.hpp"


namespace cpprisc16 {

  /* **********************************
   * Functions for IS[2] instructions *
   ************************************/
  bool
  is2_add(unsigned int result, unsigned int a, unsigned int b) {
    assert(result < nb_registers);
    assert(a < nb_registers);
    assert(b < nb_registers);

    return is1_add(result, a, b);
  }


  void
  is2_mul(unsigned int result, unsigned int a, unsigned int b) {
    assert(0 < result);
    assert(result < nb_registers);

    assert(a < nb_registers);
    assert(b < nb_registers);

    const std::uint32_t prod = (static_cast<std::uint32_t>(registers[a])
                                * static_cast<std::uint32_t>(registers[b]));

    registers[result - 1] = prod >> 16;
    registers[result] = prod;
    registers[0] = 0;

    ++nb_executed;
  }


  void
  is2_nor(unsigned int result, unsigned int a, unsigned int b) {
    assert(result < nb_registers);
    assert(a < nb_registers);
    assert(b < nb_registers);

    is1_nor(result, a, b);
  }


  bool
  is2_sha(unsigned int result, unsigned int a, unsigned int b) {
    assert(result < nb_registers);
    assert(a < nb_registers);
    assert(b < nb_registers);

    return is1_sha(result, a, b);
  }


  bool
  is2_shl(unsigned int result, unsigned int a, unsigned int b) {
    assert(result < nb_registers);
    assert(a < nb_registers);
    assert(b < nb_registers);

    return is1_shl(result, a,  b);
  }


  void
  is2_shifti(unsigned int result, unsigned int a, immed_t immed7) {
    assert(result < nb_registers);
    assert(a < nb_registers);
    assert(immed7 <= 0x7F);

    is1_shifti(result, a, immed7);
  }


  bool
  is2_sub(unsigned int result, unsigned int a, unsigned int b) {
    assert(result < nb_registers);
    assert(a < nb_registers);
    assert(b < nb_registers);

    return is1_sub(result, a, b);
  }


  void
  is2_xor(unsigned int result, unsigned int a, unsigned int b) {
    assert(result < nb_registers);
    assert(a < nb_registers);
    assert(b < nb_registers);

    is1_xor(result, a, b);
  }

}  // namespace cpprisc16
