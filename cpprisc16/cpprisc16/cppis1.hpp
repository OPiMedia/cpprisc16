/* -*- coding: latin-1 -*- */

/** \file cpprisc16/cppis1.hpp (March 15, 2017)
 * \brief Additional Instructions Set 1 IS[1]:
 * 8 new instructions is1_* and 1 instruction modified is1_add.
 *
 * Piece of cpprisc16.
 * https://bitbucket.org/OPiMedia/cpprisc16
 *
 * GPLv3 --- Copyright (C) 2017 Olivier Pirson
 * http://www.opimedia.be/
 */

#ifndef CPPRISC16_CPPRISC16_CPPIS1_HPP_
#define CPPRISC16_CPPRISC16_CPPIS1_HPP_

#include "cpprisc16.hpp"



/* *******************************
 * Macros for IS[1] instructions *
 *********************************/

/** \brief (Add Branch if Overflow)
 * R[result] <-- R[a] + R[b]
 * and if overflow
 * then jump to label.
 *
 * Count for 1 instruction.
 */
#define is1_add_bo(result, a, b, label) {       \
    assert(a < cpprisc16::nb_registers);        \
    assert(b < cpprisc16::nb_registers);        \
                                                \
    if (is1_add(result, a, b)) { goto label; }  \
  }


/** \brief (Branch if Greater)
 * If R[a] > R[b]
 * then jump to label.
 *
 * Count for 1 instruction.
 */
#define is1_bg(a, b, label) {                                           \
    assert(a < cpprisc16::nb_registers);                                \
    assert(b < cpprisc16::nb_registers);                                \
                                                                        \
    ++cpprisc16::nb_executed;                                           \
    if (cpprisc16::registers[a] > cpprisc16::registers[b]) { goto label; } \
  }


/** \brief (Branch if Lower)
 * If R[a] < R[b]
 * then jump to label.
 *
 * Count for 1 instruction.
 */
#define is1_bl(a, b, label) {                                           \
    assert(a < cpprisc16::nb_registers);                                \
    assert(b < cpprisc16::nb_registers);                                \
                                                                        \
    ++cpprisc16::nb_executed;                                           \
    if (cpprisc16::registers[a] < cpprisc16::registers[b]) { goto label; } \
  }


/** \brief (SHift Arithmetic)
 * R[result] <-- (R[a] << R[b]) or (R[a] >> -R[b])
 *
 * If R[b] >= 0
 * then shift to the left,
 * else shift to the right with *duplication of the sign bit*.
 *
 * And if overflow
 * then jump to label.
 *
 * Count for 1 instruction.
 *
 * @return true iff overflow
 */
#define is1_sha_bo(result, a, b, label) {       \
    assert(a < cpprisc16::nb_registers);        \
    assert(b < cpprisc16::nb_registers);        \
                                                \
    if (is1_sha(result, a, b)) { goto label; }  \
  }


/** \brief (SHift Logic if Overflow)
 * R[result] <-- (R[a] << R[b]) or (R[a] >> -R[b])
 *
 * If R[b] >= 0
 * then shift to the left,
 * else shift to the right.
 *
 * And if overflow
 * then jump to label.
 *
 * Count for 1 instruction.
 */
#define is1_shl_bo(result, a, b, label) {       \
    assert(a < cpprisc16::nb_registers);        \
    assert(b < cpprisc16::nb_registers);        \
                                                \
    if (is1_shl(result, a, b)) { goto label; }  \
  }


/** \brief (Sub Branch if Overflow)
 * R[result] <-- R[a] - R[b]
 * and if overflow
 * then jump to label.
 *
 * Count for 1 instruction.
 */
#define is1_sub_bo(result, a, b, label) {       \
    assert(a < cpprisc16::nb_registers);        \
    assert(b < cpprisc16::nb_registers);        \
                                                \
    if (is1_sub(result, a, b)) { goto label; }  \
  }



namespace cpprisc16 {

  /* ***********************************
   * Prototypes for IS[1] instructions *
   *************************************/
  /** \brief
   * R[result] <-- R[a] + R[b]
   *
   * See macro is1_add_bo() for jump if overflow.
   *
   * Count for 1 instruction.
   *
   * @return true iff overflow
   */
  bool
  is1_add(unsigned int result, unsigned int a, unsigned int b);


  /** \brief
   * R[result] <-- R[a] NOR R[b] (== ~(a | b))
   *
   * Count for 1 instruction.
   */
  void
  is1_nor(unsigned int result, unsigned int a, unsigned int b);


  /** \brief (SHift Arithmetic)
   * R[result] <-- (R[a] << R[b]) or (R[a] >> -R[b])
   *
   * If R[b] >= 0
   * then shift to the left,
   * else shift to the right with *duplication of the sign bit*.
   *
   * See macro is1_sha_bo() for jump if overflow.
   *
   * Count for 1 instruction.
   *
   * @return true iff overflow
   */
  bool
  is1_sha(unsigned int result, unsigned int a, unsigned int b);


  /** \brief (Shift Immediate)
   * R[result] <-- (R[a] << immed5) or (R[a] >> -immed5)
   *
   *                          6 5 43210
   * immed7 is decomposed in  x:M:immed5
   * where: x is 1 bit unused
   *        M is 1 bit: 0 for logic mode, 1 for arithmetic mode
   *        immed5 is 5 signed bits
   *
   * If immed5 >= 0
   * then shift to the left,
   * else shift to the right
   *      (with *duplication of the sign bit* if arithmetic mode).
   *
   * Count for 1 instruction.
   */
  void
  is1_shifti(unsigned int result, unsigned int a, immed_t immed7);


  /** \brief (SHift Logic)
   * R[result] <-- (R[a] << R[b]) or (R[a] >> -R[b])
   *
   * If R[b] >= 0
   * then shift to the left,
   * else shift to the right.
   *
   * See macro is1_shl_bo() for jump if overflow.
   *
   * Count for 1 instruction.
   *
   * @return true iff overflow
   */
  bool
  is1_shl(unsigned int result, unsigned int a, unsigned int b);


  /** \brief
   * R[result] <-- R[a] - R[b]
   *
   * See macro is1_sub_bo() for jump if overflow.
   *
   * Count for 1 instruction.
   *
   * @return true iff overflow
   */
  bool
  is1_sub(unsigned int result, unsigned int a, unsigned int b);


  /** \brief
   * R[result] <-- R[a] XOR R[b] (== ~(a ^ b))
   *
   * Count for 1 instruction.
   */
  void
  is1_xor(unsigned int result, unsigned int a, unsigned int b);

}  // namespace cpprisc16


#endif  // CPPRISC16_CPPRISC16_CPPIS1_HPP_
