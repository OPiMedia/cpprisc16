/* -*- coding: latin-1 -*- */

/** \file cpprisc16/cpprisc16.hpp (June 16, 2020)
 * \brief Instructions set of RiSC16:
 * 8 instructions i_* and 4 pseudo-instructions p_*.
 *
 * Piece of cpprisc16.
 * https://bitbucket.org/OPiMedia/cpprisc16
 *
 * GPLv3 --- Copyright (C) 2017, 2019, 2020 Olivier Pirson
 * http://www.opimedia.be/
 */

#ifndef CPPRISC16_CPPRISC16_CPPRISC16_HPP_
#define CPPRISC16_CPPRISC16_CPPRISC16_HPP_

#include <cassert>
#include <cstdint>



/* ******************************
 * Macro for RiSC16 instruction *
 ********************************/

/** \brief (Branch if EQual)
 * If R[a] == R[b]
 * then jump to label.
 */
#define i_beq(a, b, label) {                                            \
    assert(a < cpprisc16::nb_registers);                                \
    assert(b < cpprisc16::nb_registers);                                \
                                                                        \
    ++cpprisc16::nb_executed;                                           \
    if (cpprisc16::registers[a] == cpprisc16::registers[b]) { goto label; } \
  }



namespace cpprisc16 {

  /* *******
   * Types *
   *********/
  /** \brief
   * Type for immediate value.
   */
  typedef std::uint16_t immed_t;


  /** \brief
   * Type for register and memory items.
   */
  typedef std::uint16_t word16_t;



  /* ******************
   * Global constants *
   ********************/
  /** \brief
   * Number of registers: 8 word16_t items.
   */
  extern const unsigned int nb_registers;


  /** \brief
   * Size of the memory: 256 word16_t items.
   */
  extern const unsigned int size_memory;



  /* ******************
   * Global variables *
   ********************/
  /** \brief
   * Index following the last memory item used.
   */
  extern unsigned int after_last_memory_acceded;


  /** \brief
   * Number of instructions executed.
   */
  extern uint64_t nb_executed;


  /** \brief
   * Memory items.
   */
  extern word16_t memory[256];


  /** \brief
   * Registers.
   */
  extern word16_t registers[8];



  /* ************************************
   * Prototypes for RiSC16 instructions *
   **************************************/

  /** \brief
   * R[result] <-- R[a] + R[b]
   *
   * Count for 1 instruction.
   */
  void
  i_add(unsigned int result, unsigned int a, unsigned int b);


  /** \brief (ADD Immediate)
   * R[result] <-- R[a] + immed6
   *
   * @param result
   * @param a
   * @param immed6 <= 0x3F (== 0b111111 == 63) (size of 6 bits)
   *
   * FIXME! In fact must be immediate 7-bit signed value (-64 to 63)
   *
   * Count for 1 instruction.
   */
  void
  i_addi(unsigned int result, unsigned int a, immed_t immed6);


  /** \brief (Jump And Link using Register)
   * In the real RiSC16:
   * R[result] <-- PC + 1 (where PC = Program Counter),
   * PC        <-- R[a] (jump to R[a])
   * but impossible to implement that in this C++ library.
   *
   * Count for 1 instruction.
   *
   * @warning Not implemented!
   *
   * @param result
   * @param a
   */
  void
  i_jalr(unsigned int result, unsigned int a);


  /** \brief (Load Upper Immediate)
   * R[result] <-- immed10 << 6
   *
   * Count for 1 instruction.
   *
   * @param result
   * @param immed10 <= 0x3FF (== 0b1111111111 == 1023) (size of 10 bits)
   */
  void
  i_lui(unsigned int result, immed_t immed10);


  /** \brief (Load Word)
   * R[result] <-- Memory[R[a] + immed6]
   *
   * Count for 1 instruction.
   *
   * @param result
   * @param a
   * @param immed6 <= 0x3F (== 0b111111 == 63) (size of 6 bits)
   *
   * FIXME! In fact must be immediate 7-bit signed value (-64 to 63)
   */
  void
  i_lw(unsigned int result, unsigned int a, immed_t immed6);


  /** \brief
   * R[result] <-- R[a] NAND R[b] (== ~(a & b))
   *
   * Count for 1 instruction.
   */
  void
  i_nand(unsigned int result, unsigned int a, unsigned int b);


  /** \brief (Store Word)
   * Memory[R[result] + immed6] <-- R[a]
   *
   * Count for 1 instruction.
   *
   * @param a
   * @param result
   * @param immed6 <= 0x3F (== 0b111111 == 63) (size of 6 bits)
   *
   * FIXME! In fact must be immediate 7-bit signed value (-64 to 63)
   */
  void
  i_sw(unsigned int a, unsigned int result, immed_t immed6);



  /* *******************************************
   * Prototypes for RiSC16 pseudo-instructions *
   *********************************************/
  /** \brief
   * If print
   * then call println_all()
   *
   * Stop the program.
   *
   * Count for 1 instruction.
   */
  void
  p_halt(bool print = true);


  /** \brief (MOV Immediate)
   * R[result] <-- immed
   *
   * Pseudo-instruction for
   *   i_lui(result, immed10);
   *   i_addi(result, result, immed6);
   * with immed = immed10:immed6,
   *      immed10 = immed >> 6,
   *      immed6 = immed & 0x3F.
   *
   * Count for 2 instructions.
   */
  void
  p_movi(unsigned int result, immed_t immed);


  /** \brief
   * Do nothing.
   *
   * Pseudo-instruction for
   *   i_add(0, 0, 0);
   *
   * Count for 1 instruction.
   */
  void
  p_nop();


  /** \brief
   * In the real RiSC16:
   * R[result] <-- PC + 1 (where PC = Program Counter),
   * PC        <-- 0 (jump to 0)
   * but impossible to implement that in this C++ library.
   *
   * Pseudo-instruction for
   *   i_jalr(0, 0);
   *
   * Count for 1 instruction.
   *
   * @warning Not implemented!
   */
  void
  p_reset();


  /* ********************
   * General prototypes *
   **********************/
  /** \brief
   * Reset to 0 all memory items
   * and mark them as not used.
   */
  void
  clear_memory();


  /** \brief
   * Reset the number of executed instructions.
   */
  void
  clear_nb_executed();


  /** \brief
   * Reset to 0 all registers.
   */
  void
  clear_registers();


  /** \brief
   * Print ith memory item M[i]
   * (without newline).
   */
  void
  print_mem(unsigned int i);


  /** \brief
   * Print the register R[a]
   * (without newline).
   */
  void
  print_reg(unsigned int a);


  /** \brief
   * Print the 32 bits value of R[a2]:R[a1]
   * (without newline).
   */
  void
  print_reg2(unsigned int a2, unsigned int a1);


  /** \brief
   * Print to stdout the 16 bits value n:
   * hexadecimal representation = binary = decimal = signed decimal
   * (without newline).
   */
  void
  print_value16(std::uint16_t n);


  /** \brief 
   * Print to stdout the 32 bits value n:
   * hexadecimal representation = binary = decimal = signed decimal
   * (without newline).
   */
  void
  print_value32(std::uint32_t n);


  /** \brief
   * Print infos,
   * registers
   * and memory (if used).
   */
  void
  println_all();


  /** \brief
   * Print to stdout the number of executed instructions.
   */
  void
  println_infos();


  /** \brief
   * Print ith memory item M[i].
   */
  void
  println_mem(unsigned int i);


  /** \brief
   * Print memory items.
   *
   * If size == 0
   * then print all items until last item used,
   * else print size first items.
   */
  void
  println_memory(unsigned int size = 0);


  /** \brief
   * Print the register R[a].
   */
  void
  println_reg(unsigned int a);


  /** \brief
   * Print the 32 bits value of R[a2]:R[a1].
   */
  void
  println_reg2(unsigned int a2, unsigned int a1);


  /** \brief
   * Print all registers items.
   */
  void
  println_registers();


  /** \brief
   * Print to stdout the 16 bits value n:
   * hexadecimal representation = binary = decimal = signed decimal
   */
  void
  println_value16(std::uint16_t n);


  /** \brief
   * Print to stdout the 32 bits value n:
   * hexadecimal representation = binary = decimal = signed decimal
   */
  void
  println_value32(std::uint32_t n);

}  // namespace cpprisc16


#endif  // CPPRISC16_CPPRISC16_CPPRISC16_HPP_


/** \mainpage
 * cpprisc16
 *
 * C++ library to easily write and test "assembly" code for RiSC-16 (Ridiculously Simple Computer).
 *
 * (See <a href="https://www.ece.umd.edu/~blj/RiSC/">The RiSC-16 Architecture</a> by Bruce Jacob.)
 *
\htmlonly
<ul>
<li>
  Complete <strong>C++ sources</strong> on
  <strong><a href="https://bitbucket.org/OPiMedia/cpprisc16/">Bitbucket</a></strong>
  with a very simple example in this
  <strong><a href="https://bitbucket.org/OPiMedia/cpprisc16/src/master/cpprisc16/">README</a></strong> file
</li>
<li>
  This
  <strong><a href="http://www.opimedia.be/DS/online-documentations/cpprisc16/html/">C++ HTML online documentation</a></strong>
</li>
</ul>
\endhtmlonly
 *
 * GPLv3 &mdash; Copyright (C) 2017, 2019, 2020 Olivier Pirson
 * http://www.opimedia.be/
 */
