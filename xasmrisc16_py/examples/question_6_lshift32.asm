// xasmrisc16/question_6_lshift32.xasm (February 25, 2017)
//
// Subpiece of cpprisc16.
// https://bitbucket.org/OPiMedia/cpprisc16
//
// GPLv3 --- Copyright (C) 2017 Olivier Pirson
// http://www.opimedia.be/
//
// Question 6.
// "Write a program which shifts to the left a 32-bit value stored in reg6(MSB), reg5."
//
// r6:r5 <-- r5 << 1

movi 5, 0x1
// movi 5, 0xFFFF
movi 6, 0x2
// movi 6, 0xFFFF

// <<< IMPORT lib.xasm
// >>> IMPORT lib.xasm

// <<< INLINE lshift32, 6, 5, 1
     // %n2, %n1, %tmp <-- 6, 5, 1
     // %n2:%n1 <-- (%n2:%n1) << 1 (== 2*(%n2:%n1))
     // Precondition: %n2, %1 and %tmp different registers
     // Side effect: change %tmp

     // %tmp <-- 0 or 0b10...0 if MSB of %n1 == 0 or 1
     // <<< INLINE set0x8000, 1
         // %result <-- 1
         // %result <-- 0x8000 = 0b10...0

         lui 1, 0x200 // = 0x200 << 6
     // >>> INLINE set0x8000, 1
     // <<< INLINE and_to, 1, 5, 1
         // %result, %a, %b <-- 1, 5, 1
         // %result <-- %a bitwise and %b

         nand 1, 5, 1
         // <<< INLINE not_to, 1, 1
             // %result, %n <-- 1, 1
             // %result <-- bitwise not %n

             nand 1, 1, 1
         // >>> INLINE not_to, 1, 1
     // >>> INLINE and_to, 1, 5, 1

     // <<< INLINE lshift, 5
         // %n <-- 5
         // %n <-- %n << 1 (== 2*%n)

         add 5, 5, 5
     // >>> INLINE lshift, 5
     // <<< INLINE lshift, 6
         // %n <-- 6
         // %n <-- %n << 1 (== 2*%n)

         add 6, 6, 6
     // >>> INLINE lshift, 6

     // LSB of %n2 <-- %n2 or (%n2 + 1) if MSB of original %n1 == 0 or 1
     beq 1, 0, 0x1
     addi 6, 6, 0x1
// >>> INLINE lshift32, 6, 5, 1

halt
