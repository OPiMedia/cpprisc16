// xasmrisc16/question_7_add.xasm (February 28, 2017)
//
// Subpiece of cpprisc16.
// https://bitbucket.org/OPiMedia/cpprisc16
//
// GPLv3 --- Copyright (C) 2017 Olivier Pirson
// http://www.opimedia.be/
//
// Question 7.
// "Write a program which adds the unsigned content of reg1 and reg2
// and writes the result in reg3 and the carry bit in reg4."
//
// r4:r3 <-- r1 + r2

movi 1, 0x1
// movi 1, 0xFFFF
movi 2, 0x2
// movi 2, 0xFFFF

// <<< IMPORT lib.xasm
// >>> IMPORT lib.xasm

// <<< INLINE addc, 1, 2, 3, 4, 5
     // %a, %b, %tmp1, %tmp2, %tmp3 <-- 1, 2, 3, 4, 5
     // %b:%a <-- %a + %b
     // Precondition: %a, %b, %tmp1, %tmp2 and %tmp3 different registers
     // Side effect: change %tmp1, %tmp2 and %tmp3

     // <<< INLINE set0x8000, 5 // %tmp3 <-- mask 0b10...0
         // %result <-- 5
         // %result <-- 0x8000 = 0b10...0

         lui 5, 0x200 // = 0x200 << 6
     // >>> INLINE set0x8000, 5 // %tmp3 <-- mask 0b10...0

     nand 3, 1, 5 // %tmp2 <-- 0b11...1 or 0b01...1 if (MSB of first operand) == 1
     nand 4, 2, 5 // %tmp3 <-- 0b11...1 or 0b01...1 if (MSB of second operand) == 1

     // Set sum on 16 bits
     add 1, 1, 2 // %a <-- (first operand) + (second operand)

     nand 2, 1, 5 // b <-- 0b11...1 or 0b01...1 if (MSB of sum) == 1

     beq 3, 4, 0x1 // if MSB of operands are NOT equal
     nand 5, 2, 5 // then tmp3 <-- (0b11...1 or 0b01...1 if carry on *15* bits sum)

     // Set carry (if none pair of two MSBs 1 then jump to end)
     nand 2, 3, 4 // 0b10...0 or 0 if MSBs of %tmp1 and %tmp2 are 1
     beq 2, 0, 0x5 //               then jump to end
     nand 2, 3, 5 // 0b10...0 or 0 if MSBs of %tmp1 and %tmp3 are 1
     beq 2, 0, 0x3 //               then jump to end
     nand 2, 4, 5 // 0b10...0 or 0 if MSBs of %tmp2 and %tmp3 are 1
     beq 2, 0, 0x1 //               then jump to end

     addi 2, 0, 0x1 // %b <-- carry
// >>> INLINE addc, 1, 2, 3, 4, 5
// <<< INLINE mov, 3, 1
     // %result, %n <-- 3, 1
     // %result <-- %n

     add 3, 1, 0
// >>> INLINE mov, 3, 1
// <<< INLINE mov, 4, 2
     // %result, %n <-- 4, 2
     // %result <-- %n

     add 4, 2, 0
// >>> INLINE mov, 4, 2

halt
