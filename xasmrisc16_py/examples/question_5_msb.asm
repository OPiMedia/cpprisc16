// xasmrisc16/question_5_msb.xasm (February 28, 2017)
//
// Subpiece of cpprisc16.
// https://bitbucket.org/OPiMedia/cpprisc16
//
// GPLv3 --- Copyright (C) 2017 Olivier Pirson
// http://www.opimedia.be/
//
// Question 5.
// "Write a program which extracts the most significant bit from reg1
// and stores the value (0/1) in reg7."
//
// r7 <-- MSB of r1

movi 7, 0x1
// movi 7, 0xFFFF

// <<< IMPORT lib.xasm
// >>> IMPORT lib.xasm

// <<< INLINE msb_to, 7, 1
     // %result, %n <-- 7, 1
     // %result <-- most significant bit of %n
     // Precondition: %result and %n different registers
     // Side effect: change %n

     // %n <-- 0 or 0b10...0 if MSB == 0 or 1
     // <<< INLINE mask0x8000, 1, 7 // mask 0b10...0
         // %n, %tmp <-- 1, 7
         // %n <-- %n & 0x8000 = %n & 0b10...0
         // Pre: %n and %tmp different registers
         // Side effect: change %tmp

         // <<< INLINE set0x8000, 7
             // %result <-- 7
             // %result <-- 0x8000 = 0b10...0

             lui 7, 0x200 // = 0x200 << 6
         // >>> INLINE set0x8000, 7
         // <<< INLINE and_to, 1, 1, 7
             // %result, %a, %b <-- 1, 1, 7
             // %result <-- %a bitwise and %b

             nand 1, 1, 7
             // <<< INLINE not_to, 1, 1
                 // %result, %n <-- 1, 1
                 // %result <-- bitwise not %n

                 nand 1, 1, 1
             // >>> INLINE not_to, 1, 1
         // >>> INLINE and_to, 1, 1, 7
     // >>> INLINE mask0x8000, 1, 7 // mask 0b10...0

     // %result <- 0 or 1 if MSB == 0 or 1
     // <<< INLINE set0, 7
         // %result <-- 7
         // %result <-- 0

         add 7, 0, 0
     // >>> INLINE set0, 7
     beq 1, 0, 0x1
     addi 7, 7, 0x1
// >>> INLINE msb_to, 7, 1

halt
