// xasmrisc16/question_8_add32.xasm (February 28, 2017)
//
// Subpiece of cpprisc16.
// https://bitbucket.org/OPiMedia/cpprisc16
//
// GPLv3 --- Copyright (C) 2017 Olivier Pirson
// http://www.opimedia.be/
//
// Question 8.
// "Write a program which adds the unsigned content of 32-bit numbers stored in reg4(MSB), reg3
// and reg6(MSB), reg5 and writes the result in reg4(MSB), reg3."
//
// r4:r3 <-- r4:r3 + r6:r5

movi 3, 0x1
movi 4, 0x2
movi 5, 0x3
movi 6, 0x4

// <<< IMPORT lib.xasm
// >>> IMPORT lib.xasm

// <<< INLINE add32, 4, 3, 6, 5, 1, 2, 7
     // %a2, %a1, %b2, %b1, %tmp1, %tmp2, %tmp3 <-- 4, 3, 6, 5, 1, 2, 7
     // %a2:%a1 <-- %a2:%a1 + %b2:%b1
     // Precondition: %a2, %a1, %b2, %b1, %tmp1, %tmp2 and %tmp3 different registers
     // Side effect: change %b2, %b1, %tmp1, %tmp2 and %tmp3

     // %b2:%a1 <-- %a1 + %b1
     // <<< INLINE addc, 3, 5, 1, 2, 7
         // %a, %b, %tmp1, %tmp2, %tmp3 <-- 3, 5, 1, 2, 7
         // %b:%a <-- %a + %b
         // Precondition: %a, %b, %tmp1, %tmp2 and %tmp3 different registers
         // Side effect: change %tmp1, %tmp2 and %tmp3

         // <<< INLINE set0x8000, 7 // %tmp3 <-- mask 0b10...0
             // %result <-- 7
             // %result <-- 0x8000 = 0b10...0

             lui 7, 0x200 // = 0x200 << 6
         // >>> INLINE set0x8000, 7 // %tmp3 <-- mask 0b10...0

         nand 1, 3, 7 // %tmp2 <-- 0b11...1 or 0b01...1 if (MSB of first operand) == 1
         nand 2, 5, 7 // %tmp3 <-- 0b11...1 or 0b01...1 if (MSB of second operand) == 1

         // Set sum on 16 bits
         add 3, 3, 5 // %a <-- (first operand) + (second operand)

         nand 5, 3, 7 // b <-- 0b11...1 or 0b01...1 if (MSB of sum) == 1

         beq 1, 2, 0x1 // if MSB of operands are NOT equal
         nand 7, 5, 7 // then tmp3 <-- (0b11...1 or 0b01...1 if carry on *15* bits sum)

         // Set carry (if none pair of two MSBs 1 then jump to end)
         nand 5, 1, 2 // 0b10...0 or 0 if MSBs of %tmp1 and %tmp2 are 1
         beq 5, 0, 0x5 //               then jump to end
         nand 5, 1, 7 // 0b10...0 or 0 if MSBs of %tmp1 and %tmp3 are 1
         beq 5, 0, 0x3 //               then jump to end
         nand 5, 2, 7 // 0b10...0 or 0 if MSBs of %tmp2 and %tmp3 are 1
         beq 5, 0, 0x1 //               then jump to end

         addi 5, 0, 0x1 // %b <-- carry
     // >>> INLINE addc, 3, 5, 1, 2, 7

     // %a2 <-- %a2 + %b2 + carry
     add 4, 4, 6
     add 4, 4, 5
// >>> INLINE add32, 4, 3, 6, 5, 1, 2, 7

halt
