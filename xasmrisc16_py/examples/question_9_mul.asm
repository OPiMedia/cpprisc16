// xasmrisc16/question_9_mul.xasm (February 28, 2017)
//
// Subpiece of cpprisc16.
// https://bitbucket.org/OPiMedia/cpprisc16
//
// GPLv3 --- Copyright (C) 2017 Olivier Pirson
// http://www.opimedia.be/
//
// Question 9.
// "Write a program which multiplies the (unsigned) content of reg1 and reg2
// and writes the result into reg4(MSB), reg3."
//
// r4:r3 <-- r1 * r2

movi 1, 0x123
movi 2, 0x3

// <<< IMPORT lib.xasm
// >>> IMPORT lib.xasm

// <<< INLINE mul, 1, 2, 3, 4, 5, 6, 7
     // %a, %b, %r1, %r2, %tmp1, %tmp2, %tmp3 <-- 1, 2, 3, 4, 5, 6, 7
     // %r2:%r1 <-- %a * %b
     // by standard algorithm:
     // https://en.wikipedia.org/wiki/Multiplication_algorithm#Long_multiplication
     // Precondition: %a, %b, %r1, %r2, %tmp2 and %tmp3 different registers
     // Side effect: change %r1, %r2, %tmp2 and %tmp3
     //              Use memory 0x0 to 0x3 (included) to save temporary work.


     // If first or second operand == 0 then jump to mul_end
     beq 1, 0, mul_end
     beq 2, 0, mul_end


     // Memory 0: first operand (least significant bits)
     //        1: first operand (most significant bits)
     //        2: second operand
     //        3: mask


     // %r2:%r1 will contain the product
     // <<< INLINE set0, 3
         // %result <-- 3
         // %result <-- 0

         add 3, 0, 0
     // >>> INLINE set0, 3
     // <<< INLINE set0, 4
         // %result <-- 4
         // %result <-- 0

         add 4, 0, 0
     // >>> INLINE set0, 4


     // %tmp1 <-- second operand (%b), and save
     // <<< INLINE mov, 5, 2
         // %result, %n <-- 5, 2
         // %result <-- %n

         add 5, 2, 0
     // >>> INLINE mov, 5, 2
     sw 5, 0, 0x2 // save second operand


     // %b:%a will contain first operand on 32 bits, shifted on each step
     // <<< INLINE set0, 2
         // %result <-- 2
         // %result <-- 0

         add 2, 0, 0
     // >>> INLINE set0, 2


     // %tmp3 <-- mask = 1
     addi 7, 0, 0x1


     // For each bit on the second operand,
     // if the current bit is 1
     // then add the shifted first operand to the partial product.
     mul_loop: // nop
         // INV: %b:%a == shifted first operand
         //      %r2:%r1 == partial product (sum of some shifted first operand)
         //      %tmp1 == second operand
         //      %tmp3 == mask

         // (Bit of second operand) == 0 or 1 ?
         // <<< INLINE and_to, 6, 5, 7
             // %result, %a, %b <-- 6, 5, 7
             // %result <-- %a bitwise and %b

             nand 6, 5, 7
             // <<< INLINE not_to, 6, 6
                 // %result, %n <-- 6, 6
                 // %result <-- bitwise not %n

                 nand 6, 6, 6
             // >>> INLINE not_to, 6, 6
         // >>> INLINE and_to, 6, 5, 7
         beq 6, 0, mul_endif_bit

         // If (bit of second operand) == 1
             sw 1, 0, 0x0 // save first operand
             sw 2, 0, 0x1
             sw 7, 0, 0x3 // save mask

             // %r2:%r1 <-- %r2:%r1 + %b:%a
             // <<< INLINE add32, 4, 3, 2, 1, 6, 5, 7
                 // %a2, %a1, %b2, %b1, %tmp1, %tmp2, %tmp3 <-- 4, 3, 2, 1, 6, 5, 7
                 // %a2:%a1 <-- %a2:%a1 + %b2:%b1
                 // Precondition: %a2, %a1, %b2, %b1, %tmp1, %tmp2 and %tmp3 different registers
                 // Side effect: change %b2, %b1, %tmp1, %tmp2 and %tmp3

                 // %b2:%a1 <-- %a1 + %b1
                 // <<< INLINE addc, 3, 1, 6, 5, 7
                     // %a, %b, %tmp1, %tmp2, %tmp3 <-- 3, 1, 6, 5, 7
                     // %b:%a <-- %a + %b
                     // Precondition: %a, %b, %tmp1, %tmp2 and %tmp3 different registers
                     // Side effect: change %tmp1, %tmp2 and %tmp3

                     // <<< INLINE set0x8000, 7 // %tmp3 <-- mask 0b10...0
                         // %result <-- 7
                         // %result <-- 0x8000 = 0b10...0

                         lui 7, 0x200 // = 0x200 << 6
                     // >>> INLINE set0x8000, 7 // %tmp3 <-- mask 0b10...0

                     nand 6, 3, 7 // %tmp2 <-- 0b11...1 or 0b01...1 if (MSB of first operand) == 1
                     nand 5, 1, 7 // %tmp3 <-- 0b11...1 or 0b01...1 if (MSB of second operand) == 1

                     // Set sum on 16 bits
                     add 3, 3, 1 // %a <-- (first operand) + (second operand)

                     nand 1, 3, 7 // b <-- 0b11...1 or 0b01...1 if (MSB of sum) == 1

                     beq 6, 5, 0x1 // if MSB of operands are NOT equal
                     nand 7, 1, 7 // then tmp3 <-- (0b11...1 or 0b01...1 if carry on *15* bits sum)

                     // Set carry (if none pair of two MSBs 1 then jump to end)
                     nand 1, 6, 5 // 0b10...0 or 0 if MSBs of %tmp1 and %tmp2 are 1
                     beq 1, 0, 0x5 //               then jump to end
                     nand 1, 6, 7 // 0b10...0 or 0 if MSBs of %tmp1 and %tmp3 are 1
                     beq 1, 0, 0x3 //               then jump to end
                     nand 1, 5, 7 // 0b10...0 or 0 if MSBs of %tmp2 and %tmp3 are 1
                     beq 1, 0, 0x1 //               then jump to end

                     addi 1, 0, 0x1 // %b <-- carry
                 // >>> INLINE addc, 3, 1, 6, 5, 7

                 // %a2 <-- %a2 + %b2 + carry
                 add 4, 4, 2
                 add 4, 4, 1
             // >>> INLINE add32, 4, 3, 2, 1, 6, 5, 7

             lw 5, 0, 0x2 // reload second operand
             lw 1, 0, 0x0 // reload first operand
             lw 2, 0, 0x1
             lw 7, 0, 0x3 // reload mask
         mul_endif_bit: // nop

         // mask <-- mask << 1
         // <<< INLINE lshift, 7
             // %n <-- 7
             // %n <-- %n << 1 (== 2*%n)

             add 7, 7, 7
         // >>> INLINE lshift, 7
         beq 7, 0, mul_end

         // first operand <-- (first operand) << 1
         // <<< INLINE lshift32, 2, 1, 6
             // %n2, %n1, %tmp <-- 2, 1, 6
             // %n2:%n1 <-- (%n2:%n1) << 1 (== 2*(%n2:%n1))
             // Precondition: %n2, %1 and %tmp different registers
             // Side effect: change %tmp

             // %tmp <-- 0 or 0b10...0 if MSB of %n1 == 0 or 1
             // <<< INLINE set0x8000, 6
                 // %result <-- 6
                 // %result <-- 0x8000 = 0b10...0

                 lui 6, 0x200 // = 0x200 << 6
             // >>> INLINE set0x8000, 6
             // <<< INLINE and_to, 6, 1, 6
                 // %result, %a, %b <-- 6, 1, 6
                 // %result <-- %a bitwise and %b

                 nand 6, 1, 6
                 // <<< INLINE not_to, 6, 6
                     // %result, %n <-- 6, 6
                     // %result <-- bitwise not %n

                     nand 6, 6, 6
                 // >>> INLINE not_to, 6, 6
             // >>> INLINE and_to, 6, 1, 6

             // <<< INLINE lshift, 1
                 // %n <-- 1
                 // %n <-- %n << 1 (== 2*%n)

                 add 1, 1, 1
             // >>> INLINE lshift, 1
             // <<< INLINE lshift, 2
                 // %n <-- 2
                 // %n <-- %n << 1 (== 2*%n)

                 add 2, 2, 2
             // >>> INLINE lshift, 2

             // LSB of %n2 <-- %n2 or (%n2 + 1) if MSB of original %n1 == 0 or 1
             beq 6, 0, 0x1
             addi 2, 2, 0x1
         // >>> INLINE lshift32, 2, 1, 6

     // Jump to mul_loop
     beq 0, 0, mul_loop

     mul_end: // nop
// >>> INLINE mul, 1, 2, 3, 4, 5, 6, 7

halt
