// xasmrisc16/swap.xasm (March 14, 2017)
//
// Very simple example: swap values of two registers.
//
// Subpiece of cpprisc16.
// https://bitbucket.org/OPiMedia/cpprisc16
//
// GPLv3 --- Copyright (C) 2017 Olivier Pirson
// http://www.opimedia.be/
//
// r1, r2 <-- r2, r1

movi 1, 0x42
movi 2, 0x666

// <<< IMPORT lib.xasm
// >>> IMPORT lib.xasm

// <<< INLINE swap, 1, 2, 3 // used swap "function" from lib.xasm
     // %a, %b, %tmp <-- 1, 2, 3
     // %a, %b <-- %b, %a
     // Side effect: change %tmp

     // <<< INLINE mov, 3, 1
         // %result, %n <-- 3, 1
         // %result <-- %n

         add 3, 1, 0
     // >>> INLINE mov, 3, 1
     // <<< INLINE mov, 1, 2
         // %result, %n <-- 1, 2
         // %result <-- %n

         add 1, 2, 0
     // >>> INLINE mov, 1, 2
     // <<< INLINE mov, 2, 3
         // %result, %n <-- 2, 3
         // %result <-- %n

         add 2, 3, 0
     // >>> INLINE mov, 2, 3
// >>> INLINE swap, 1, 2, 3 // used swap "function" from lib.xasm

halt
