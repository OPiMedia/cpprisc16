#!/bin/bash

../xasmrisc16.py --no-nop $1 swap.xasm > swap.asm
echo '----------'

../xasmrisc16.py --no-nop $1 question_4_lshift.xasm > question_4_lshift.asm
echo '----------'
../xasmrisc16.py --no-nop $1 question_5_msb.xasm > question_5_msb.asm
echo '----------'
../xasmrisc16.py --no-nop $1 question_6_lshift32.xasm > question_6_lshift32.asm
echo '----------'
../xasmrisc16.py --no-nop $1 question_7_add.xasm > question_7_add.asm
echo '----------'
../xasmrisc16.py --no-nop $1 question_8_add32.xasm > question_8_add32.asm
echo '----------'
../xasmrisc16.py --no-nop $1 question_9_mul.xasm > question_9_mul.asm
