// xasmrisc16/question_4_lshift.xasm (February 25, 2017)
//
// Subpiece of cpprisc16.
// https://bitbucket.org/OPiMedia/cpprisc16
//
// GPLv3 --- Copyright (C) 2017 Olivier Pirson
// http://www.opimedia.be/
//
// Question 4.
// "Write a program which shifts to the left the content of reg5."
//
// r5 <-- r5 << 1

movi 5, 0x1
// movi 5, 0xFFFF

// <<< IMPORT lib.xasm
// >>> IMPORT lib.xasm

// <<< INLINE lshift_to, 5, 5
     // %result, %n <-- 5, 5
     // %result <-- %n << 1 (== 2*%n)

     add 5, 5, 5
// >>> INLINE lshift_to, 5, 5

halt
