.. -*- restructuredtext -*-

=============
xasmrisc16.py
=============
Subpiece of cpprisc16.

Quick and dirty "compiler" writed in Python
from **"extended"** RiSC-16 assembler to RiSC-16 assembler.

Print to standard output the conversion of XASM to ASM.

(See `The RiSC-16 Architecture`_ by Bruce Jacob.)

.. _`The RiSC-16 Architecture`: https://www.ece.umd.edu/~blj/RiSC/

* `xasmrisc16.py`_
* `examples/`_

.. _`xasmrisc16.py`: https://bitbucket.org/OPiMedia/cpprisc16/src/master/xasmrisc16_py/xasmrisc16.py
.. _`examples/`: https://bitbucket.org/OPiMedia/cpprisc16/src/master/xasmrisc16_py/examples/

|



Very simple example: swap values of two registers.
==================================================
Instead this strict RiSC16 assembler code:

.. code-block::

    movi 1, 0x42
    movi 2, 0x666

    // r1, r2 <-- r2, r1   (change r3)
    add 3, 1, 0
    add 1, 2, 0
    add 2, 3, 0

    halt

You can define a "function" in a **separated** file (like `lib.asm`):

.. code-block::

    FUNCTION swap, %a, %b, %tmp
        // %a, %b <-- %b, %a
        // Side effect: change %tmp

        INLINE mov, %tmp, %a
        INLINE mov, %a, %b
        INLINE mov, %b, %tmp
    ENDFUNCTION

And then, after imported this file, you can use the "function" like this:

.. code-block::

    movi 1, 0x42
    movi 2, 0x666

    IMPORT lib.xasm

    INLINE swap, 1, 2, 3  // used swap "function" from lib.xasm

    halt

:warning:
    If you use label in a function, you can only use this function once.

|



Author: 🌳 Olivier Pirson — OPi |OPi| 🇧🇪🇫🇷🇬🇧 🐧 👨‍💻 👨‍🔬
=================================================================
🌐 Website: http://www.opimedia.be/

💾 Bitbucket: https://bitbucket.org/OPiMedia/

* 📧 olivier.pirson.opi@gmail.com
* Mastodon: https://mamot.fr/@OPiMedia — Twitter: https://twitter.com/OPirson
* 👨‍💻 LinkedIn: https://www.linkedin.com/in/olivierpirson/ — CV: http://www.opimedia.be/CV/English.html
* other profiles: http://www.opimedia.be/about/

.. |OPi| image:: http://www.opimedia.be/_png/OPi.png

|



License: GPLv3_ |GPLv3|
=======================
Copyright (C) 2017 Olivier Pirson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.

.. _GPLv3: https://www.gnu.org/licenses/gpl-3.0.html

.. |GPLv3| image:: https://www.gnu.org/graphics/gplv3-88x31.png
