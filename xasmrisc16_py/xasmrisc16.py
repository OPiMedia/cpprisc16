#!/usr/bin/env python
# -*- coding: latin-1 -*-

"""
xasmrisc16.py (March 12, 2017)

Quick and dirty "compiler"
from "extended" RiSC-16 assembler to RiSC-16 assembler.

Print to standard output the conversion of XASM to ASM.

Usage: xasmrisc16.py [--no-nop|--nop] filename.xasm
  --no-nop: remove NOP pseudo-instructions
  --nop: keep NOP pseudo-instructions

Subpiece of cpprisc16.
https://bitbucket.org/OPiMedia/cpprisc16

GPLv3 --- Copyright (C) 2017 Olivier Pirson
http://www.opimedia.be/

Started February 17, 2017
"""

from __future__ import division
from __future__ import print_function

import collections
import re
import sys


####################
# Global constants #
####################
EXTENSIONS = {'ENDFUNCTION': (0, 0),
              'FUNCTION': (1, 9),
              'IMPORT': (1, 1),
              'INLINE': (1, 9)}
# TODO: ARRAY, CALL, MACRO...

INSTRUCTIONS = {'ADD': 3,
                'add': 3,
                'ADDI': 3,
                'addi': 3,
                'NAND': 3,
                'nand': 3,
                'LUI': 2,
                'lui': 2,
                'LW': 3,
                'lw': 3,
                'SW': 3,
                'sw': 3,
                'BEQ': 3,
                'beq': 3,
                'JALR': 2,
                'jalr': 2}

PSEUDOS = {'HALT': 0,
           'halt': 0,
           'MOVI': 2,
           'movi': 2,
           'NOP': 0,
           'nop': 0,
           'RESET': 0,
           'reset': 0}

PSEUDOS_NB_INSTRUCTIONS = {'HALT': 1,
                           'halt': 1,
                           'MOVI': 2,
                           'movi': 2,
                           'NOP': 1,
                           'nop': 1,
                           'RESET': 1,
                           'reset': 1}


####################
# Global variables #
####################
INLINED = {}

NO_NOP = False


###########
# Classes #
###########
class Line:
    @staticmethod
    def str_to_line(line_number, str_line):
        assert isinstance(line_number, int), type(line_number)
        assert line_number >= 1, line_number

        assert isinstance(str_line, str), type(str_line)

        indent = ''
        label = ''
        opcode = ''
        comment = ''

        str_line = str_line.rstrip()

        # indent
        match = re.match(r'(\s+)(.*)$', str_line)
        if match:
            indent = match.group(1)
            str_line = match.group(2)

        # comment
        match = re.match(r'(.*?)\s*(//.*)$', str_line)
        if match:
            str_line = match.group(1)
            comment = match.group(2)

        # label
        match = re.match(r'(.*?:)\s*(.*)$', str_line)
        if match:
            label = match.group(1)
            str_line = match.group(2)

        # opcode
        match = re.match(r'(.*?)\s+(.*)$', str_line)
        if match:
            opcode = match.group(1)
            str_line = match.group(2)
        else:
            opcode = str_line
            str_line = ''

        # params
        params = (re.split(r',\s*', str_line) if str_line
                  else [])

        if NO_NOP and (opcode in ('NOP', 'nop')):
            comment = '// ' + opcode + ' ' + comment
            opcode = ''

        return Line(line_number, indent, label, opcode, params, comment)

    def __init__(self, line_number, indent, label, opcode, params, comment,
                 filename=''):
        assert isinstance(line_number, int), type(line_number)
        assert line_number >= 0, line_number
        # assert line_number >= 1, line_number

        assert isinstance(indent, str), type(indent)
        assert isinstance(label, str), type(label)
        assert isinstance(opcode, str), type(opcode)
        assert isinstance(params, collections.Iterable), type(params)
        assert isinstance(comment, str), type(comment)

        self.__line_number = line_number
        self.__indent = indent
        self.__label = label
        self.__opcode = opcode
        self.__params = tuple(params)
        self.__comment = comment
        self.__filename = filename

        if not NO_NOP and label and not opcode:
            error(self.__filename, self.__line_number,
                  'Opcode is mandatory with label')

        assert self._is_correct(), repr(self)

    def __repr__(self):
        return ("{}({}, '{}', '{}', '{}', {}, '{}', filename='{}')"
                .format(self.__class__.__name__,
                        self.__line_number, self.__indent, self.__label,
                        self.__opcode, str(self.__params), self.__comment,
                        self.__filename))

    def __str__(self):
        seq = []
        if self.__indent:
            seq.append(self.__indent)
        if self.__label:
            seq.append(self.__label)
        if self.__opcode:
            seq.append(self.__opcode)
        if self.__params:
            seq.append(', '.join(self.__params))
        if self.__comment:
            seq.append(self.__comment)

        return ' '.join(seq).rstrip()

    def _is_correct(self):
        if self.is_instruction():
            for instruction, number_params in INSTRUCTIONS.items():
                if self.__opcode == instruction:
                    return len(self.__params) == number_params

            assert False

        if self.is_pseudo():
            for pseudo, number_params in PSEUDOS.items():
                if self.__opcode == pseudo:
                    return len(self.__params) == number_params

            assert False

        if self.is_extension():
            for extension, number_params in EXTENSIONS.items():
                if self.__opcode.upper() == extension:
                    return (number_params[0] <= len(self.__params)
                            <= number_params[1])

            assert False

        return self.is_empty() or self.has_label() or self.has_comment()

    def comment(self):
        return self.__comment

    def has_comment(self):
        return self.__comment

    def has_label(self):
        return self.__label

    def indent(self):
        return self.__indent

    def is_function_begin(self):
        return self.__opcode == 'FUNCTION'

    def is_function_end(self):
        return self.__opcode == 'ENDFUNCTION'

    def is_empty(self):
        return not(self.__indent or self.__label
                   or self.__opcode or self.__params or self.__comment)

    def is_extension(self):
        for extension in EXTENSIONS:
            if self.__opcode == extension:
                return True

        return False

    def is_instruction(self):
        for instruction in INSTRUCTIONS:
            if self.__opcode == instruction:
                return True

        return False

    def is_pseudo(self):
        for pseudo in PSEUDOS:
            if self.__opcode.upper() == pseudo:
                return True

        return False

    def label(self):
        return self.__label

    def line_number(self):
        return self.__line_number

    def nb_instructions(self):
        return (1 if self.is_instruction()
                else (PSEUDOS_NB_INSTRUCTIONS[self.__opcode]
                      if self.is_pseudo()
                      else 0))

    def opcode(self):
        return self.__opcode

    def params(self):
        return tuple(self.__params)

    def set_filename(self, filename):
        assert isinstance(filename, str), type(filename)

        self.__filename = filename

    def to_asm(self):
        if not self.is_extension():
            return Prog((self, ))

        prog = Prog()

        prog.append(Line(self.__line_number, self.__indent, self.__label,
                         '', '',
                         '// <<< {} {} {}'.format(self.__opcode,
                                                  ', '.join(self.__params),
                                                  self.__comment),
                         filename=self.__filename))

        if self.__opcode == 'IMPORT':
            prog.extend(self.__import_to_functions())
        elif self.__opcode == 'INCLUDE':
            prog.extend(self.__include_to_asm())
        elif self.__opcode == 'INLINE':
            prog.extend(self.__inline_to_asm())
        else:
            assert False

        prog.append(Line(self.__line_number, self.__indent, self.__label,
                         '', '',
                         '// >>> {} {} {}'.format(self.__opcode,
                                                  ', '.join(self.__params),
                                                  self.__comment),
                         filename=self.__filename))

        return prog

    def __import_to_functions(self):
        lib = Prog()

        filename = self.__params[0]
        try:
            with open(filename) as fin:
                lib.extend_load(fin)
        except IOError:
            error(self.__filename, self.__line_number,
                  'File "{}" NOT found: {}'.format(filename, str(self)))

        lib.set_filename(filename)

        prog = Prog()

        function_name = None
        body = Prog()
        params = None
        for line in lib:
            if line.is_function_begin():  # FUNCTION
                if function_name:
                    error(line.__filename, line.__line_number, '???')

                function_name = line.__params[0]
                params = line.__params[1:]
                if Prog.has_function(function_name):
                    error(self.__filename, self.__line_number,
                          'FUNCTION "{}" already defined'
                          .format(function_name))

            elif line.is_function_end():  # ENDFUNCTION
                if not function_name:
                    error(line.__filename, line.__line_number, '???')

                prog.set_function(function_name, params, body)

                function_name = None
                body = Prog()
                params = None
            elif function_name:           # body of the function
                body.append(line)
            else:                         # outside function
                pass  # prog.append(line)

        if function_name:
            error(self.__filename, self.__line_number,
                  'FUNCTION "{}" NOT ended'.format(function_name))

        return prog

    def __include_to_asm(self):
        prog = Prog()

        filename = self.__params[0]
        try:
            with open(filename) as fin:
                prog.extend_load(fin)
        except IOError:
            error(self.__filename, self.__line_number,
                  'File "{}" NOT found: {}'.format(filename, str(self)))

        prog.set_filename(filename)

        return prog

    def __inline_to_asm(self):
        function_name, params = self.__params[0], self.__params[1:]

        if function_name not in INLINED:
            INLINED[function_name] = 0
        INLINED[function_name] += 1

        function_params, function_body = Prog.function(function_name)

        prog = Prog()
        prog.append(Line(0, self.__indent + '    ', '', '', tuple(),
                         '// {} <-- {}'.format(', '.join(function_params),
                                               ', '.join(params))))

        if len(params) != len(function_params):
            error(self.__filename, self.__line_number,
                  'FUNCTION "{}" ???'.format(function_name))

        for line in function_body:
            new_params = []
            for line_param in line.__params:
                new_param = None
                for i, function_param in enumerate(function_params):
                    if line_param == function_param:
                        new_param = params[i]

                        break

                new_params.append(line_param if new_param is None
                                  else new_param)

            new_indent = (line.__indent if line.is_empty()
                          else self.__indent + line.__indent)

            prog.append(Line(line.__line_number,
                             new_indent,
                             line.__label, line.__opcode,
                             new_params,
                             line.__comment, line.__filename))

        return prog.to_asm()


class Prog:
    __functions = dict()

    @classmethod
    def function(cls, function_name):
        assert isinstance(function_name, str), type(function_name)

        if not Prog.has_function(function_name):
            error('??? filename', 6661,
                  'FUNCTION "{}" NOT defined'.format(function_name))

        return Prog.__functions[function_name]

    @classmethod
    def function_nb_instructions(cls, function_name):
        assert isinstance(function_name, str), type(function_name)

        number = 0
        for line in cls.function(function_name)[1].to_asm():
            number += line.nb_instructions()

        return number

    @classmethod
    def has_function(cls, function_name):
        assert isinstance(function_name, str), type(function_name)

        return function_name in Prog.__functions

    def __init__(self, lines=None):
        assert ((lines is None)
                or isinstance(lines, collections.Iterable)), type(lines)

        self.__lines = (list() if lines is None
                        else list(lines))

        if __debug__:
            for line in self:
                assert isinstance(line, Line), type(line)

    def __iter__(self):
        return iter(self.__lines)

    def __len__(self):
        return len(self.__lines)

    def __repr__(self):
        return '\n'.join(repr(line) for line in self)

    def __str__(self):
        return '\n'.join(str(line) for line in self)

    def append(self, line):
        assert isinstance(line, Line), type(line)

        self.__lines.append(line)

    def extend(self, prog):
        assert isinstance(prog, Prog), type(prog)

        self.__lines.extend(prog)

    def extend_load(self, fin):
        """
        ???
        Instruction and pseudo-instruction:
        [label:] opcode [param1[, param2[, filed3]]] [// comment]
        """
        for i, str_line in enumerate(fin):
            line = Line.str_to_line(i + 1, str_line)
            self.__lines.append(line)

    def set_filename(self, filename):
        assert isinstance(filename, str), type(filename)

        for line in self:
            line.set_filename(filename)

    def set_function(self, function_name, params, body):
        assert isinstance(function_name, str), type(function_name)
        assert isinstance(params, collections.Iterable), type(params)
        assert isinstance(body, Prog), type(body)

        assert not Prog.has_function(function_name), function_name

        params = tuple(params)

        for param in params:
            assert isinstance(param, str), type(param)

            if not is_meta_param(param):
                error('', 6662, '???' + param)

        params_set = set(params)
        for line in body:
            for param in line.params():
                if is_meta_param(param) and (param not in params_set):
                    error('', 6663, '???' + param + str(line.params()))

        Prog.__functions[function_name] = (params, body)

    def to_asm(self):
        prog = Prog()

        for line in self:
            prog.extend(line.to_asm())

        return prog


#############
# Functions #
#############
def error(filename, line_number, msg):
    assert isinstance(filename, str), type(filename)

    assert isinstance(line_number, int), type(line_number)
    assert line_number >= 1, line_number

    assert isinstance(msg, str), type(msg)

    print('!Error in{} line {}: {}'
          .format(('file {}'.format(filename) if filename
                   else ''),
                  line_number, msg), file=sys.stderr)

    exit(1)


def help():
    print("""xasmrisc16.py (February 22, 2017)

Print to standard output the conversion of XASM to ASM.

Usage: xasmrisc16.py [--no-nop|--nop] filename.xasm
  --no-nop: remove NOP pseudo-instructions
  --nop: keep NOP pseudo-instructions

GPLv3 --- Copyright (C) 2017 Olivier Pirson
http://www.opimedia.be/""", file=sys.stderr)

    exit(1)


def is_meta_param(param):
    assert isinstance(param, str), type(param)

    return (len(param) >= 2) and (param[0] == '%')


########
# Main #
########
def main():
    # Params
    filename = None

    global NO_NOP

    for param in sys.argv[1:]:
        if param == '--no-nop':
            NO_NOP = True
        elif param == '--nop':
            NO_NOP = False
        else:
            filename = param

    if filename is None:
        help()

    # Compile
    prog = Prog()
    with (sys.stdin if filename == '-'
          else open(filename)) as fin:
        prog.extend_load(fin)

    # Print
    prog = prog.to_asm()
    print(str(prog))
    # print(repr(prog), file=sys.stderr)
    if __debug__:
        print(('Removed' if NO_NOP
               else 'Kept'),
              'NOP pseudo-instructions', file=sys.stderr)
        for function_name, number in sorted(INLINED.items()):
            print('{}:\tsize={}\tinlined #={}'
                  .format(function_name,
                          Prog.function_nb_instructions(function_name),
                          number),
                  file=sys.stderr)


if __name__ == '__main__':
    main()
